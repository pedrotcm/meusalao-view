import { Client } from '../../../_models/client';
import { Order } from '../../../_models/order';
import { Professional } from '../../../_models/professional';
import { Service } from '../../../_models/service';
import { ServiceSelected } from '../../../_models/serviceSelected';
import { Status } from '../../../_models/status';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { SelectItem, SelectItemGroup } from 'primeng/components/common/api';
import { ORDERS, CLIENTS, PROFESSIONALS, PT_BR, SERVICES, CATEGORIES } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { CurrencyPipe } from '@angular/common';

@Component( {
    selector: 'app-order-open',
    templateUrl: './order-open.component.html',
    styleUrls: [ './order-open.component.scss' ],
    animations: [ routerTransition ],
    providers: [ CurrencyPipe ]
} )
export class OrderOpenComponent implements OnInit {

    results: Order[];
    datasource: Order[];
    cols: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;
    displayPayment = false;
    isEdit = false;
    cadastreForm: FormGroup;
    timeTotalPrevision: number;
    subtotalValue: number;

    filtered: any[];
    filteredClients: any[];
    filteredServices: any[];
    services: SelectItemGroup[] = [];

    professionals: Professional[];
    professionalsFilter: Professional[];
    clients: Client[];
    professionalSelected: Professional;

    colsServices: any[];
    ptBR: any;
    prices: any[];

    constructor( private fb: FormBuilder, private currencyPipe: CurrencyPipe ) { }

    ngOnInit() {
        this.ptBR = PT_BR;
        this.datasource = ORDERS.slice( 0 );
        this.clients = CLIENTS.slice( 0 );
        this.professionals = PROFESSIONALS.slice( 0 );
        this.cols = [
            { field: 'orderNumber', header: 'Comanda' },
            { field: 'clientName', header: 'Cliente' },
            { field: 'dateOpened', header: 'Data de Abertura' }
        ];
        this.cadastreForm = this.fb.group( {
            'id': new FormControl(),
            'orderNumber': new FormControl(),
            'client': new FormControl( null, Validators.required ),
            'services': this.fb.array( [ this.createServiceItem() ] ),
            'dateOpened': new FormControl(),
            'status': new FormControl( Status.OPENED ),
            'valueTotal': new FormControl()
        } );

        this.servicesItens.valueChanges.subscribe( value => {
            this.timeTotalPrevision = 0;
            this.subtotalValue = 0;
            value.forEach( v => {
                if ( v.service != null ) {
                    this.timeTotalPrevision += v.service.timePrediction;
                }
                if ( v.price != null ) {
                    this.subtotalValue += v.price.value;
                }
            } );
        } );

        CATEGORIES.slice( 0 ).forEach( c => {
            const items = [];
            c.services.forEach( s => {
                items.push(
                    { label: s.name, value: s }
                );
            } );
            this.services.push( {
                label: c.description, value: c,
                items: items
            } );
        } );

    }

    createServiceItem(): FormGroup {
        return this.fb.group( {
            service: new FormControl( null, Validators.required ),
            professionals: this.fb.array( [ this.createProfessionalItem() ] ),
            price: new FormControl( null, Validators.required )
        } );
    }

    createProfessionalItem(): FormGroup {
        return this.fb.group( {
            professional: new FormControl( null, Validators.required )
        } );
    }

    get servicesItens(): FormArray {
        return this.cadastreForm.get( 'services' ) as FormArray;
    }


    addServiceItem() {
        this.servicesItens.push( this.createServiceItem() );
    }

    removeServiceItem() {
        this.servicesItens.removeAt( this.servicesItens.length - 1 );
    }

    addProfessionalItem( index ) {
        const professionals = this.servicesItens.controls[ index ].get( 'professionals' ) as FormArray;
        professionals.push( this.createProfessionalItem() );
    }

    removeProfessionalItem( indexService, indexProfessional ) {
        const professionals = this.servicesItens.controls[ indexService ].get( 'professionals' ) as FormArray;
        professionals.removeAt( indexProfessional );
    }

    edit( order: Order ) {
        this.timeTotalPrevision = order.timeTotalPrevision;
        this.subtotalValue = order.valueTotal;

        const services = [];
        order.services.forEach( s => {
            const professionals = [];
            s.professionals.forEach( p => {
                professionals.push( { professional: p } );
            } );

            services.push(
                {
                    price: s.price,
                    service: s.service,
                    professionals: professionals
                }
            );
        } );

        this.cadastreForm.patchValue( {
            id: order.id,
            orderNumber: order.orderNumber,
            dateOpened: order.dateOpened,
            status: order.status,
            services: services,
            valueTotal: this.subtotalValue
        } );
        this.cadastreForm.controls[ 'client' ].reset( { value: order.client, disabled: true } );

        this.isEdit = true;
        this.display = true;
    }

    remove( order: Order ) {

    }

    loadLazy( event: LazyLoadEvent ) {
        this.loading = true;

        // in a real application, make a remote request to load data using state metadata from event
        // event.first = First row offset
        // event.rows = Number of rows per page
        // event.sortField = Field name to sort with
        // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        // filters: FilterMetadata object having field as key and filter value, filter matchMode as value

        // imitate db connection over a network
        setTimeout( () => {
            if ( this.datasource ) {
                const filteredRows = this.datasource.filter( row => this.filterField( row, event.filters ) );
                filteredRows.sort( ( a, b ) => this.compareField( a, b, event.sortField ) * event.sortOrder );
                this.results = filteredRows.slice( event.first, ( event.first + event.rows ) );
                this.firstRegister = event.first + 1;
                this.rowsPerPage = event.first + this.results.length;
                this.totalRecords = filteredRows.length;
            }
            this.loading = false;
        }, 1000 );
    }

    filterField( row, filter ) {
        let isInFilter = false;
        let noFilter = true;

        for ( const columnName in filter ) {
            for ( let i = 0; i < this.cols.length; i++ ) {
                const column = this.cols[ i ];
                if ( row[ column.field ] == null ) {
                    continue;
                }
                noFilter = false;
                const rowValue: String = row[ column.field ].toString().toLowerCase();
                const filterMatchMode: String = filter[ columnName ].matchMode;
                if ( filterMatchMode.includes( 'contains' ) && rowValue.includes( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'startsWith' ) && rowValue.startsWith( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'in' ) && filter[ columnName ].value.includes( rowValue ) ) {
                    isInFilter = true;
                }
            }
        }
        if ( noFilter ) {
            isInFilter = true;
        }
        return isInFilter;
    }

    compareField( rowA, rowB, field: string ): number {
        if ( rowA[ field ] == null ) {
            return 1; // on considère les éléments null les plus petits
        }
        if ( typeof rowA[ field ] === 'string' ) {
            return rowA[ field ].localeCompare( rowB[ field ] );
        }
        if ( typeof rowA[ field ] === 'number' ) {
            if ( rowA[ field ] > rowB[ field ] ) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    onHideDialog( e ) {
        this.display = false;
        //        this.displayChange.emit( this.display );
    }

    filter( event ) {
        this.filtered = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filtered = [];
        }
        for ( let i = 0; i < this.professionals.length; i++ ) {
            if ( this.professionals[ i ].name.toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filtered.push( this.professionals[ i ] );
            }
        }
    }

    filterClients( event ) {
        this.filteredClients = [];
        if ( event.query.toLowerCase() === '' ) {
            this.filteredClients = [];
        }
        for ( let i = 0; i < this.clients.length; i++ ) {
            if ( this.clients[ i ].name.toLowerCase().indexOf( event.query.toLowerCase() ) === 0 ) {
                this.filteredClients.push( this.clients[ i ] );
            }
        }
    }

    onSelectProfessional( professional: Professional ): any {
        this.cadastreForm.controls[ 'professional' ].reset( { value: professional, disabled: true } );
    }

    clearAutoComplete( event ) {
        this.professionalSelected = null;
    }

    clearAutoCompleteClient( event ) {
        this.cadastreForm.controls[ 'client' ].reset();
    }

    onSubmit( { value, valid }: { value: Order, valid: boolean } ) {
        value.timeTotalPrevision = this.timeTotalPrevision;
        value.valueTotal = this.subtotalValue;
        value.dateOpened = new Date();
        console.log( value );
        if ( valid ) {
            this.display = false;
        }
    }

    getPrices( rowIndex ): SelectItem[] {
        const pricesSelect: SelectItem[] = [];
        const prices = this.servicesItens.controls[ rowIndex ].get( 'service' ).value.prices;
        if ( prices.length === 1 ) {
            this.servicesItens.controls[ rowIndex ].patchValue( {
                price: prices[ 0 ]
            } );
            pricesSelect.push( { label: this.currencyPipe.transform( prices[ 0 ].value, 'BRL' ), value: prices[ 0 ] } );
            return pricesSelect;
        }

        prices.forEach( p => {
            pricesSelect.push( { label: p.description + ' - ' + this.currencyPipe.transform( p.value, 'BRL' ), value: p } );
        } );
        return pricesSelect;
    }

}
