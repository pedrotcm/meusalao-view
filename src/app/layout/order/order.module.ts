import { MinuteToHour } from '../../_shared/pipers/minuteToHour';
import { MinuteToHourModule } from '../../_shared/pipers/minuteToHour.module';
import { OrderRoutingModule } from './order-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TabViewModule } from 'primeng/tabview';
import { OrderOpenComponent } from './order-open/order-open.component';
import { OrderCloseComponent } from './order-close/order-close.component';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule as CalendarPrimengModule } from 'primeng/calendar';
import { CalendarModule } from 'angular-calendar';
import { PaymentComponent } from './payment/payment.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { OrderPendentComponent } from './order-pendent/order-pendent.component';

@NgModule( {
    imports: [
        CommonModule,
        OrderRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TooltipModule,
        ConfirmDialogModule,
        TabViewModule,
        DialogModule,
        DropdownModule,
        CalendarModule,
        CalendarPrimengModule,
        MinuteToHourModule,
        CurrencyMaskModule
    ],
    declarations: [ OrderComponent, OrderOpenComponent, OrderCloseComponent, PaymentComponent, OrderPendentComponent ]
} )
export class OrderModule { }
