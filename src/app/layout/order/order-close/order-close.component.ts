import { Order } from '../../../_models/order';
import { Status } from '../../../_models/status';
import { routerTransition } from '../../../router.animations';
import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/components/common/api';

@Component( {
    selector: 'app-order-close',
    templateUrl: './order-close.component.html',
    styleUrls: [ './order-close.component.scss' ],
    animations: [ routerTransition ],
    providers: [ CurrencyPipe ]
} )
export class OrderCloseComponent implements OnInit {

    results: Order[];
    datasource: Order[];
    cols: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;
    isEdit = false;
    orderForm: FormGroup;

    constructor( private fb: FormBuilder, private currencyPipe: CurrencyPipe ) { }

    ngOnInit() {
        this.cols = [
            { field: 'orderNumber', header: 'Comanda' },
            { field: 'clientName', header: 'Cliente' },
            { field: 'dateOpened', header: 'Data de Abertura' },
            { field: 'dateClosed', header: 'Data de Fechamento' }
        ];
        this.orderForm = this.fb.group( {
            'id': new FormControl(),
            'orderNumber': new FormControl(),
            'client': new FormControl( null, Validators.required ),
            //            'services': this.fb.array( [ this.createServiceItem() ] ),
            'dateOpened': new FormControl(),
            'dateClosed': new FormControl(),
            'status': new FormControl( Status.FINISHED ),
            'valueTotal': new FormControl()
        } );
    }

    loadLazy( event: LazyLoadEvent ) {
        this.loading = true;

        // in a real application, make a remote request to load data using state metadata from event
        // event.first = First row offset
        // event.rows = Number of rows per page
        // event.sortField = Field name to sort with
        // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        // filters: FilterMetadata object having field as key and filter value, filter matchMode as value

        // imitate db connection over a network
        setTimeout( () => {
            if ( this.datasource ) {
                const filteredRows = this.datasource.filter( row => this.filterField( row, event.filters ) );
                filteredRows.sort( ( a, b ) => this.compareField( a, b, event.sortField ) * event.sortOrder );
                this.results = filteredRows.slice( event.first, ( event.first + event.rows ) );
                this.firstRegister = event.first + 1;
                this.rowsPerPage = event.first + this.results.length;
                this.totalRecords = filteredRows.length;
            }
            this.loading = false;
        }, 1000 );
    }

    filterField( row, filter ) {
        let isInFilter = false;
        let noFilter = true;

        for ( const columnName in filter ) {
            for ( let i = 0; i < this.cols.length; i++ ) {
                const column = this.cols[ i ];
                if ( row[ column.field ] == null ) {
                    continue;
                }
                noFilter = false;
                const rowValue: String = row[ column.field ].toString().toLowerCase();
                const filterMatchMode: String = filter[ columnName ].matchMode;
                if ( filterMatchMode.includes( 'contains' ) && rowValue.includes( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'startsWith' ) && rowValue.startsWith( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'in' ) && filter[ columnName ].value.includes( rowValue ) ) {
                    isInFilter = true;
                }
            }
        }
        if ( noFilter ) {
            isInFilter = true;
        }
        return isInFilter;
    }

    compareField( rowA, rowB, field: string ): number {
        if ( rowA[ field ] == null ) {
            return 1; // on considère les éléments null les plus petits
        }
        if ( typeof rowA[ field ] === 'string' ) {
            return rowA[ field ].localeCompare( rowB[ field ] );
        }
        if ( typeof rowA[ field ] === 'number' ) {
            if ( rowA[ field ] > rowB[ field ] ) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    edit( order: Order ) {

        const services = [];
        order.services.forEach( s => {
            const professionals = [];
            s.professionals.forEach( p => {
                professionals.push( { professional: p } );
            } );

            services.push(
                {
                    price: s.price,
                    service: s.service,
                    professionals: professionals
                }
            );
        } );

        this.orderForm.patchValue( {
            id: order.id,
            orderNumber: order.orderNumber,
            dateOpened: order.dateOpened,
            status: order.status,
            services: services,
            //            valueTotal: this.subtotalValue
        } );
        this.orderForm.controls[ 'client' ].reset( { value: order.client, disabled: true } );

        this.isEdit = true;
        this.display = true;
    }

    remove( order: Order ) {

    }


}
