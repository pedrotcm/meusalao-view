import { Order } from '../../../_models/order';
import { PaymentMethod } from '../../../_models/paymentMethod';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { PAYMENT_METHODS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
// import {
//    debounceTime, distinctUntilChanged, switchMap
// } from 'rxjs/operators';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@Component( {
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: [ './payment.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class PaymentComponent implements OnInit {

    @Input()
    displayPayment: boolean;

    @Input()
    displayOrder: boolean;

    @Output()
    displayPaymentChange = new EventEmitter();

    @Output()
    displayOrderChange = new EventEmitter();

    @Input()
    orderForm: FormGroup;

    paymentMethods: PaymentMethod[];
    amountPaid = 0;
    missingValue = 0;
    changeValue = 0;

    constructor( private fb: FormBuilder ) { }

    ngOnInit() {
        this.paymentMethods = PAYMENT_METHODS.slice( 0 );
        this.orderForm.addControl( 'paymentMethods', this.fb.array( [ this.createPaymentMethodItem() ] ) );
        this.paymentMethodsItens.valueChanges.debounceTime( 500 )
            .distinctUntilChanged()
            .subscribe( paymentMethods => {
                this.amountPaid = 0;
                paymentMethods.forEach( payment => {
                    this.amountPaid += payment.value;
                } );
                const valueTotal = this.orderForm.get( 'valueTotal' ).value;
                this.missingValue = this.amountPaid < valueTotal ? valueTotal - this.amountPaid : 0;
                this.changeValue = this.amountPaid > valueTotal ? this.amountPaid - valueTotal : 0;
            } );

        this.paymentMethodsItens.at( 0 ).patchValue( {
            method: this.paymentMethods[ 0 ]
        } );
    }

    createPaymentMethodItem(): FormGroup {
        const valuePayment = new FormControl( null, Validators.required );
        return this.fb.group( {
            method: new FormControl( null, Validators.required ),
            value: valuePayment
        } );
    }

    get paymentMethodsItens(): FormArray {
        return this.orderForm.get( 'paymentMethods' ) as FormArray;
    }


    addPaymentMethod() {
        this.paymentMethodsItens.push( this.createPaymentMethodItem() );
    }

    removePaymentMethod() {
        this.paymentMethodsItens.removeAt( this.paymentMethodsItens.length - 1 );
    }


    onHideDialog( e ) {
        this.displayPayment = false;
        this.displayOrder = true;
        this.displayOrderChange.emit( this.displayOrder );
        this.displayPaymentChange.emit( this.displayPayment );
        console.log( this.orderForm.getRawValue() );
    }

    onSubmit( { value, valid }: { value: Order, valid: boolean } ) {
        if ( valid ) {
            this.displayPayment = false;
            this.displayOrder = false;
            this.displayOrderChange.emit( this.displayOrder );
            this.displayPaymentChange.emit( this.displayPayment );
        }
    }

    onSubmitTeste() {
        console.log( this.orderForm );
    }

}
