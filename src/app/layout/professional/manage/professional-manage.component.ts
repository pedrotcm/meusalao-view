import { Professional } from '../../../_models/professional';
import { ProfessionalService } from '../../../_services/professional.service';
import { DatatableFilterService } from '../../../_shared/services/datatablefilter.service';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { PROFESSIONALS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { Table } from 'primeng/components/table/table';
import { ConfirmationService } from 'primeng/api';

@Component( {
    selector: 'app-professional-manage',
    templateUrl: './professional-manage.component.html',
    styleUrls: [ './professional-manage.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ConfirmationService ]
} )
export class ProfessionalManageComponent implements OnInit {

    results: Professional[];
    datasource: Professional[];
    first = 0;
    cols: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;

    lastEvent: LazyLoadEvent;
    editEntity: Professional;
    removeEntity: number;

    constructor(
        private confirmationService: ConfirmationService,
        private professionalService: ProfessionalService,
        private tableFilterService: DatatableFilterService
    ) { }

    ngOnInit() {
        this.loading = true;
        this.cols = [
            { field: 'name', header: 'Nome' },
            { field: 'birthday', header: 'Aniversário' },
            { field: 'phone', header: 'Telefone' },
            { field: 'email', header: 'E-mail' },
            //            { field: 'rg', header: 'RG' },
            //            { field: 'cpf', header: 'CPF' },
            { field: 'contractType', header: 'Tipo de Contrato' },
            { field: 'justArrivalOrder', header: 'Apenas Ordem de Chegada' },
        ];
    }

    edit( editEntity: Professional ) {
        this.editEntity = editEntity;
    }

    remove( removeEntity: Professional ) {
        this.confirmationService.confirm( {
            header: 'Confirmar exclusão',
            message: 'Deseja realmente excluir este profissional?',
            icon: 'fa fa-trash',
            accept: () => {
                this.removeEntity = removeEntity.id;
            },
            reject: () => {
            }
        } );
    }

    loadLazy( event: LazyLoadEvent ) {
        this.lastEvent = event;
        if ( !this.datasource ) {
            this.getProfessionals();
        }

        if ( this.datasource ) {
            this.tableFilterService.filterProcess( this, event );
        }
    }

    updateTable( update ) {
        if ( update ) {
            this.loading = true;
            this.getProfessionals();
        }
    }

    private getProfessionals() {
        this.professionalService.getProfessionals()
            .finally( () => this.loading = false )
            .subscribe( data => {
                this.datasource = data;
                this.tableFilterService.filterProcess( this, this.lastEvent );
            } );
    }
}
