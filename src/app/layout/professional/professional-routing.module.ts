import { ProfessionalComponent } from './professional.component';
import { ProfessionalCadastreComponent } from './cadastre/professional-cadastre.component';
import { ProfessionalManageComponent } from './manage/professional-manage.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ProfessionalComponent,
        children: [
            { path: '', component: ProfessionalManageComponent },
            { path: 'cadastre', component: ProfessionalCadastreComponent },
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ProfessionalRoutingModule { }
