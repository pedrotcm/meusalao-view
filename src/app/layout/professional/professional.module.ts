import { CategoryService } from '../../_services/category.service';
import { ProfessionalService } from '../../_services/professional.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfessionalComponent } from './professional.component';
import { ProfessionalManageComponent } from './manage/professional-manage.component';
import { ProfessionalCadastreComponent } from './cadastre/professional-cadastre.component';
import { ProfessionalRoutingModule } from './professional-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputMaskModule } from 'primeng/inputmask';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { PickListModule } from 'primeng/picklist';
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { ListboxModule } from 'primeng/listbox';
import { CheckboxModule } from 'primeng/checkbox';

@NgModule( {
    imports: [
        CommonModule,
        ProfessionalRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        InputMaskModule,
        TableModule,
        ButtonModule,
        TooltipModule,
        ConfirmDialogModule,
        InputMaskModule,
        AutoCompleteModule,
        InputSwitchModule,
        DropdownModule,
        FieldsetModule,
        PickListModule,
        DialogModule,
        AccordionModule,
        ListboxModule,
        CheckboxModule
    ],
    declarations: [ ProfessionalComponent, ProfessionalManageComponent, ProfessionalCadastreComponent ],
    providers: [ ProfessionalService, CategoryService ]
} )
export class ProfessionalModule { }
