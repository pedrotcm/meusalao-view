import { Category } from '../../../_models/category';
import { Professional } from '../../../_models/professional';
import { Service } from '../../../_models/service';
import { CategoryService } from '../../../_services/category.service';
import { ProfessionalService } from '../../../_services/professional.service';
import { MsgsService } from '../../../_shared/services/msgs.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { STATES, SERVICES, CATEGORIES } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, ViewChild, TemplateRef, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { stringify } from 'querystring';
import { SelectItem } from 'primeng/components/common/api';

@Component( {
    selector: 'app-professional-cadastre',
    templateUrl: './professional-cadastre.component.html',
    styleUrls: [ './professional-cadastre.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class ProfessionalCadastreComponent implements OnInit, OnChanges {

    @Input()
    display = false;

    @Output()
    displayChange = new EventEmitter();

    @Output()
    updateTableChange = new EventEmitter();

    @Input()
    editEntity: Professional;

    @Output()
    editEntityChange = new EventEmitter();

    @Input()
    removeEntity: number;

    @Output()
    removeEntityChange = new EventEmitter();

    cadastreForm: FormGroup;
    states: SelectItem[];
    typeContracts: any[];
    categories: Category[];
    dialogTitle: string;

    @ViewChild( 'dialogContent' ) public dialog: TemplateRef<any>;

    constructor(
        private fb: FormBuilder,
        private validateFormService: ValidateFormService,
        private professionalService: ProfessionalService,
        private modalService: NgbModal,
        private categoryService: CategoryService
    ) { }

    init() {
        this.cadastreForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'nickname': new FormControl( null, Validators.required ),
            'contractType': new FormControl( null, Validators.required ),
            'justArrivalOrder': new FormControl(),
            'birthday': new FormControl( null, Validators.required ),
            'phone': new FormControl( null, Validators.required ),
            'email': new FormControl(),
            'address': this.fb.group( {
                'id': new FormControl(),
                'address': new FormControl( null, Validators.required ),
                'zipCode': new FormControl( null, Validators.required ),
                'neighborhood': new FormControl( null, Validators.required ),
                'city': new FormControl( null, Validators.required ),
                'state': new FormControl( null, Validators.required )
            } ),
            'rg': new FormControl( null, Validators.required ),
            'cpf': new FormControl( null, Validators.required ),
            'services': this.fb.array( [] )
        } );
        this.categoryService.getCategories().subscribe( data => {
            this.categories = data.filter( category => {
                if ( category && category.services.length > 0 ) {
                    return category;
                }
            } );
            this.categories.forEach( category => {
                const services = this.services;
                const serviceGroup = this.fb.group( {
                    'serviceGroup': new FormControl(),
                } );
                services.push( serviceGroup );
            } );

        } );
        this.cadastreForm.patchValue( {
            'justArrivalOrder': false
        } );
        this.validateFormService.form = this.cadastreForm;
    }

    get services(): FormArray {
        return this.cadastreForm.get( 'services' ) as FormArray;
    }

    ngOnInit() {
        this.init();
        //        this.categories = CATEGORIES.slice( 0 );
        this.typeContracts = [ { label: 'Comissão', value: 'COMISSION' }, { label: 'Valor Fixo', value: 'FIXED_VALUE' }];
        this.states = STATES;
    }

    //    clear( pickList ) {
    //        this.cadastreForm.reset();
    //        this.cadastreForm.patchValue( {
    //            'justArrivalOrder': false
    //        }
    //        );
    //        this.targetList = [];
    //        pickList.source = SERVICES.slice( 0 );
    //        this.validateFormService.clear();
    //    }

    onHideDialog( e ) {
        this.display = false;
        this.displayChange.emit( this.display );
    }

    ngOnChanges( changes: SimpleChanges ) {
        if ( changes[ 'display' ] ) {
            const change = changes[ 'display' ];
            const curVal = JSON.stringify( change.currentValue );
            if ( curVal === 'true' ) {
                this.dialogTitle = this.editEntity ? 'Editar Profissional' : 'Novo Profissional';
                this.modalService.open( this.dialog, { centered: true, size: 'lg', keyboard: false, backdrop: 'static' } ).result.then( ( result ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );
                }, ( reason ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );
                } );
            }
        } else if ( changes[ 'editEntity' ] ) {
            this.cadastreForm.patchValue( this.editEntity );
            this.categories.forEach( ( category, idx ) => {
                const values = [];
                this.editEntity.services.forEach( service => {
                    if ( this.contains( category.services, service ) ) {
                        values.push( service );
                    }
                } );
                this.services.controls[ idx ].patchValue( {
                    serviceGroup: values
                } );
            } );
            this.displayChange.emit( true );
        } else if ( changes[ 'removeEntity' ] ) {
            this.professionalService.deleteProfessional( this.removeEntity )
                .subscribe( result => {
                    this.updateTableChange.emit( true );
                } );
        }
    }

    private contains( array, item ) {
        for ( let i = 0; i < array.length; i++ ) {
            if ( array[ i ].id === item.id ) {
                return true;
            }
        }
        return false;
    }

    onChangeServiceSelected( e ) {
        //        const services = this.cadastreForm.get( 'services' ) as FormArray;
        //        services.push( new FormControl( e.value ) );
        //        e.value.forEach( v => {
        //            const index = services.controls.findIndex( c => {
        //                return c.value === v;
        //            } );
        //            if ( index >= 0 ) {
        //                services.controls.splice( index, 1 );
        //            } else {
        //                services.push( new FormControl( v ) );
        //            }
        //        } );

        //        console.log( services.controls );
        //        console.log( services, this.cadastreForm.value, index );
    }

    onSubmit( { value, valid }: { value: Professional, valid: boolean }, close ) {
        if ( valid ) {
            this.professionalService.addProfessional( value ).subscribe( result => {
                console.log( 'Result', result );
                if ( result ) {
                    this.init();
                    close();
                    this.updateTableChange.emit( true );
                }
            } );
        }
    }

}
