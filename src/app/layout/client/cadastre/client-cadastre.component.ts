import { Address } from '../../../_models/address';
import { Client } from '../../../_models/client';
import { ClientService } from '../../../_services/client.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { STATES, CHANNELS_MET_SALON } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, SimpleChange, OnChanges, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem } from 'primeng/components/common/api';
import * as $ from 'jquery';

@Component( {
    selector: 'app-client-cadastre',
    templateUrl: './client-cadastre.component.html',
    styleUrls: [ './client-cadastre.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class ClientCadastreComponent implements OnInit, OnDestroy {

    @Input()
    display = false;

    @Output()
    displayChange = new EventEmitter();

    @Output()
    updateTableChange = new EventEmitter();

    @Input()
    editEntity: Client;

    @Output()
    editEntityChange = new EventEmitter();

    @Input()
    removeEntity: number;

    @Output()
    removeEntityChange = new EventEmitter();

    @ViewChild( 'dialogContent' ) public dialog: TemplateRef<any>;

    dialogTitle: string;

    cadastreForm: FormGroup;
    filtered: any[];
    states: SelectItem[];
    channelsMetSalon: any[];

    dialogElement = null;
    targetElement = null;


    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private modalService: NgbModal,
        private clientService: ClientService
    ) { }

    init() {
        this.cadastreForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'birthday': new FormControl( null, Validators.required ),
            'phone': new FormControl( null, Validators.required ),
            'phone2': new FormControl(),
            'email': new FormControl(),
            'channelMetSalon': new FormControl(),
            'address': this.fb.group( {
                'address': new FormControl(),
                'zipCode': new FormControl(),
                'neighborhood': new FormControl(),
                'city': new FormControl(),
                'state': new FormControl()
            } ),
            'observations': new FormControl()
        } );
        this.validateFormService.form = this.cadastreForm;
    }

    ngOnInit() {
        this.init();
        this.states = STATES;
        this.channelsMetSalon = CHANNELS_MET_SALON;
    }

    ngOnChanges( changes: { [ display: string ]: SimpleChange } ) {
        if ( changes[ 'display' ] ) {
            const change = changes[ 'display' ];
            const curVal = JSON.stringify( change.currentValue );
            if ( curVal === 'true' ) {
                this.dialogTitle = this.editEntity ? 'Editar Cliente' : 'Novo Cliente';
                this.modalService.open( this.dialog, { centered: true, size: 'lg', keyboard: false, backdrop: 'static' } ).result.then( ( result ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );
                }, ( reason ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );

                } );
            }
        } else if ( changes[ 'editEntity' ] ) {
            this.cadastreForm.patchValue( this.editEntity );
            this.displayChange.emit( true );
        } else if ( changes[ 'removeEntity' ] ) {
            this.clientService.deleteClient( this.removeEntity )
                .subscribe( result => {
                    this.updateTableChange.emit( true );
                } );
        }
    }

    clear() {
        this.ngOnInit();
        this.validateFormService.clear();
    }

    onShowDialog( e ) {
        //        this.displayChange.emit( true );
    }

    onHideDialog( e ) {
        this.displayChange.emit( false );
    }

    onSubmit( { value, valid }: { value: Client, valid: boolean }, close ) {
        if ( valid ) {
            this.clientService.addClient( value ).subscribe( data => {
                if ( data ) {
                    this.init();
                    close();
                    this.updateTableChange.emit( true );
                }
            } );
            //            this.displayChange.emit( false );
        }
        //         else {
        //            this.validateFormService.displayFieldErrors();
        //        }
    }

    clearAutoComplete( event ) {
        this.cadastreForm.controls[ 'address' ].get( 'state' ).reset();
    }

    ngOnDestroy() {
        this.displayChange.unsubscribe();
    }
}
