import { ClientService } from '../../_services/client.service';
import { MsgsModule } from '../../_shared/modules/messages/msgs.module';
import { ClientManageComponent } from './manage/client-manage.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './client.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ClientRoutingModule } from './client-routing.module';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ClientCadastreComponent } from './cadastre/client-cadastre.component';
import { InputMaskModule } from 'primeng/inputmask';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';

@NgModule( {
    imports: [
        CommonModule,
        ClientRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TooltipModule,
        ConfirmDialogModule,
        InputMaskModule,
        DialogModule,
        DropdownModule,
        MsgsModule
    ],
    declarations: [ ClientComponent, ClientManageComponent, ClientCadastreComponent ],
    providers: [ ClientService ]
} )
export class ClientModule { }
