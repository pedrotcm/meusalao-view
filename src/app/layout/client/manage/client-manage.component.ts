import { Client } from '../../../_models/client';
import { ClientService } from '../../../_services/client.service';
import { DatatableFilterService } from '../../../_shared/services/datatablefilter.service';
import { CLIENTS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { ConfirmationService } from 'primeng/api';

@Component( {
    selector: 'app-client-manage',
    templateUrl: './client-manage.component.html',
    styleUrls: [ './client-manage.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ConfirmationService ]
} )
export class ClientManageComponent implements OnInit {

    results: Client[];
    datasource: any[];
    cols: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;

    lastEvent: LazyLoadEvent;
    editEntity: Client;
    removeEntity: number;

    constructor(
        private confirmationService: ConfirmationService,
        private tableFilterService: DatatableFilterService,
        private clientService: ClientService
    ) { }

    ngOnInit() {
        this.cols = [
            { field: 'name', header: 'Nome' },
            { field: 'birthday', header: 'Aniversário' },
            { field: 'phone', header: 'Telefone' },
            { field: 'email', header: 'E-mail' }
        ];
    }

    edit( editEntity: Client ) {
        this.editEntity = editEntity;
    }

    remove( removeEntity: Client ) {
        this.confirmationService.confirm( {
            header: 'Confirmar exclusão',
            message: 'Caso o cliente possua agendamentos, estes também serão excluídos. Deseja realmente excluir este cliente?',
            icon: 'fa fa-trash',
            accept: () => {
                this.removeEntity = removeEntity.id;
            },
            reject: () => {
            }
        } );
    }

    updateTable( update ) {
        if ( update ) {
            this.loading = true;
            this.getClients();
        }
    }

    loadLazy( event: LazyLoadEvent ) {
        this.lastEvent = event;
        if ( !this.datasource ) {
            this.getClients();
        }

        if ( this.datasource ) {
            this.tableFilterService.filterProcess( this, event );
        }
    }

    private getClients() {
        this.clientService.getClients()
            .finally( () => this.loading = false )
            .subscribe( data => {
                this.datasource = data;
                this.tableFilterService.filterProcess( this, this.lastEvent );
            } );
    }


}
