import { ClientCadastreComponent } from './cadastre/client-cadastre.component';
import { ClientManageComponent } from './manage/client-manage.component';
import { ClientComponent } from './client.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ClientComponent,
        children: [
            { path: '', component: ClientManageComponent }
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ClientRoutingModule { }
