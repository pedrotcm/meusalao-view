import { AuthGuardService } from '../_guards/authguard.service';
import { RoleGuardService } from '../_guards/roleguard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'clients', loadChildren: './client/client.module#ClientModule' },
            { path: 'services', loadChildren: './service/service.module#ServiceModule' },
            {
                path: 'professionals', loadChildren: './professional/professional.module#ProfessionalModule',
                canActivate: [ AuthGuardService, RoleGuardService ], data: {
                    expectedRole: [ 'MANAGE_PROFESSIONALS' ]
                }
            },
            { path: 'schedule', loadChildren: './schedule/scheduling.module#SchedulingModule' },
            { path: 'orders', loadChildren: './order/order.module#OrderModule' }
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class LayoutRoutingModule { }
