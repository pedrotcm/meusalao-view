import { AuthGuardService } from '../_guards/authguard.service';
import { RoleGuardService } from '../_guards/roleguard.service';
import { SecuredDirective } from '../_shared/directives/secured.directive';
import { MsgsModule } from '../_shared/modules/messages/msgs.module';
import { DatatableFilterService } from '../_shared/services/datatablefilter.service';
import { MsgsService } from '../_shared/services/msgs.service';
import { ValidateFormService } from '../_shared/services/validate-form.service';
import { LayoutRoutingModule } from './layout-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './_components/header/header.component';
import { SidebarComponent } from './_components/sidebar/sidebar.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule( {
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule.forRoot(),
        MsgsModule,
        NgbModule.forRoot()
    ],
    declarations: [ LayoutComponent, HeaderComponent, SidebarComponent, SecuredDirective ],
    providers: [ ValidateFormService, MsgsService, DatatableFilterService, AuthGuardService, RoleGuardService ]
} )
export class LayoutModule { }
