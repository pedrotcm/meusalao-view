import { Component, OnInit } from '@angular/core';
import { animate, style, transition, trigger, state } from '@angular/animations';
import { Message } from 'primeng/components/common/api';
import * as $ from 'jquery';

@Component( {
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: [ './layout.component.scss' ],
    animations: [
        trigger( 'slideInMainContainer', [
            state( 'true', style( {
                marginLeft: '0'
            } ) ),
            transition( 'true => false', [
                style( { marginLeft: '0' } ),
                animate( '0.25s ease-in-out', style( { marginLeft: '*' } ) )
            ] ),
            transition( 'false => true', [
                style( { marginLeft: '*' } ),
                animate( '0.25s ease-in-out', style( { marginLeft: '0' } ) )
            ] )
        ] )
    ]
} )
export class LayoutComponent implements OnInit {

    sidebarInactiveClass = 'sidebar-inactive';
    dialogIsOpened;
    scrollpos;

    constructor() {
        console.log( 'contructor' );
        this.dialogIsOpened = false;
    }

    ngDoCheck() {
        const dom: Element = document.querySelector( 'body' );
        if ( dom.classList.contains( 'modal-open' ) && !this.dialogIsOpened ) {
            console.log( 'open' );
            this.dialogIsOpened = true;
            this.storeBodyScroll();
        } else {
            if ( !dom.classList.contains( 'modal-open' ) && this.dialogIsOpened ) {
                console.log( 'close' );
                this.dialogIsOpened = false;
                this.scrollOriginPosition();
            }
        }
    }

    ngOnInit() {
        const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test( navigator.platform );
        const dom: Element = document.querySelector( 'body' );
        if ( iOS ) {
            dom.className += 'ios';
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector( 'body' );
        return dom.classList.contains( this.sidebarInactiveClass );
    }


    storeBodyScroll() {
        this.scrollpos = $( window ).scrollTop();
        $( 'body' ).css( { top: -this.scrollpos } );
        $( 'body' ).css( { position: 'fixed' } );
    }

    scrollOriginPosition() {
        // Scroll to it if it is and remove held position
        $( 'body' ).css( { position: 'relative' } );
        if ( !!this.scrollpos && this.scrollpos * -1 < 0 ) {
            $( 'body' ).css( { top: 0 } );
            window.scrollTo( 0, this.scrollpos );
        }
    }
}
