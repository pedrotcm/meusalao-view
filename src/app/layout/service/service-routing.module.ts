import { ServiceCadastreComponent } from './cadastre/service-cadastre.component';
import { ServiceManageComponent } from './manage/service-manage.component';
import { ServiceComponent } from './service.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ServiceComponent,
        children: [
            { path: '', component: ServiceManageComponent },
            { path: 'cadastre', component: ServiceCadastreComponent },
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ServiceRoutingModule { }
