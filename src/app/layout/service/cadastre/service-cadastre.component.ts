import { Category } from '../../../_models/category';
import { Professional } from '../../../_models/professional';
import { Service } from '../../../_models/service';
import { Status } from '../../../_models/status';
import { CategoryService } from '../../../_services/category.service';
import { ProfessionalService } from '../../../_services/professional.service';
import { ServiceService } from '../../../_services/service.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { TYPE_VALUES, CATEGORIES } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component( {
    selector: 'app-service-cadastre',
    templateUrl: './service-cadastre.component.html',
    styleUrls: [ './service-cadastre.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ValidateFormService ]
} )
export class ServiceCadastreComponent implements OnInit, OnChanges {

    @Input()
    display = false;

    @Output()
    displayChange = new EventEmitter();

    @Output()
    updateTableChange = new EventEmitter();

    @Input()
    editEntity: Service;

    @Output()
    editEntityChange = new EventEmitter();

    @Input()
    removeEntity: number;

    @Output()
    removeEntityChange = new EventEmitter();

    @ViewChild( 'dialogContent' ) public dialog: TemplateRef<any>;

    cadastreForm: FormGroup;
    categories = [];
    typesValue: any[];
    colsServices: any[];
    professionals: Professional[];
    selectedProfessionals: Professional[] = [];
    dialogTitle: string;


    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        private categoryService: CategoryService,
        private serviceService: ServiceService,
        private professionalService: ProfessionalService,
        private modalService: NgbModal
    ) { }

    init() {
        this.typesValue = TYPE_VALUES;
        this.cadastreForm = this.fb.group( {
            'id': new FormControl(),
            'name': new FormControl( null, Validators.required ),
            'commission': new FormControl( null, Validators.required ),
            'timePrediction': new FormControl( null, Validators.required ),
            'visibleToClient': new FormControl(),
            'category': new FormControl( null, Validators.required ),
            'typeValue': new FormControl(),
            'valueUnique': new FormControl( null, Validators.required ),
            'idValueUnique': new FormControl(),
            'professionals': [ [] ],
            'prices': this.fb.array( [] )
        } );
        this.cadastreForm.patchValue( {
            'visibleToClient': true,
            'typeValue': TYPE_VALUES[ 0 ].value,
        } );
        this.validateFormService.form = this.cadastreForm;
    }

    ngOnInit() {
        this.init();
        this.professionalService.getProfessionals().subscribe( data => {
            this.professionals = data;
        } );
        this.categoryService.getCategories().subscribe( data => {
            for ( const category of data ) {
                delete category.services;
                this.categories.push( {
                    label: category.description, value: category
                } );
            }
        } );
        this.colsServices = [
            { field: 'description', header: 'Complemento do Nome' },
            { field: 'value', header: 'Valor' }
        ];
        //        const typeValueChanges = this.cadastreForm.controls.typeValue.valueChanges;
    }

    typeValueOnChange( event ) {
        if ( event.value === 'VARIED' ) {
            this.cadastreForm.patchValue( {
                'valueUnique': null
            } );
            this.cadastreForm.controls.valueUnique.clearValidators();
            //            this.cadastreForm.addControl(
            //                'valuesVaried', this.fb.array( [ this.createPrice() ] )
            //            );
            this.addPrice();
        } else {
            this.cadastreForm.controls.valueUnique.setValidators( [ Validators.required ] );
            this.cadastreForm.removeControl( 'prices' );
            this.cadastreForm.addControl(
                'prices', this.fb.array( [] )
            );
            this.prices.clearValidators();
        }
        this.cadastreForm.controls.valueUnique.updateValueAndValidity();
    }

    ngOnChanges( changes: SimpleChanges ) {
        if ( changes[ 'display' ] ) {
            const change = changes[ 'display' ];
            const curVal = JSON.stringify( change.currentValue );
            if ( curVal === 'true' ) {
                this.dialogTitle = this.editEntity ? 'Editar Serviço' : 'Novo Serviço';
                this.modalService.open( this.dialog, { centered: true, size: 'lg', keyboard: false, backdrop: 'static' } ).result.then( ( result ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );
                }, ( reason ) => {
                    this.init();
                    this.displayChange.emit( false );
                    this.editEntityChange.emit( null );
                } );
            }
        } else if ( changes[ 'editEntity' ] ) {
            this.editEntity.prices.forEach( ( price, idx ) => {
                if ( price.complementOfName === null ) {
                    this.cadastreForm.patchValue( {
                        idValueUnique: price.id,
                        valueUnique: price.value
                    } );
                } else {
                    this.cadastreForm.controls.valueUnique.clearValidators();
                    this.addPrice();
                }
            } );
            this.cadastreForm.patchValue( this.editEntity );
            console.log( this.editEntity, this.cadastreForm );
            this.displayChange.emit( true );
        } else if ( changes[ 'removeEntity' ] ) {
            this.serviceService.deleteService( this.removeEntity )
                .subscribe( result => {
                    this.updateTableChange.emit( true );
                } );
        }
    }

    createPrice(): FormGroup {
        return this.fb.group( {
            id: new FormControl(),
            complementOfName: new FormControl( null, Validators.required ),
            value: new FormControl( null, Validators.required )
        } );
    }

    get prices(): FormArray {
        return this.cadastreForm.get( 'prices' ) as FormArray;
    }

    addPrice() {
        this.prices.push( this.createPrice() );
    }

    removePrice() {
        this.prices.removeAt( this.prices.length - 1 );
        console.log( this.prices.length );
    }

    onSubmit( { value, valid }: { value: Service, valid: boolean }, close ) {
        console.log( value, valid, this.cadastreForm );
        if ( valid ) {
            this.serviceService.addService( value ).subscribe( data => {
                if ( data ) {
                    this.init();
                    close();
                    this.updateTableChange.emit( true );
                }
            } );
        }
        //         else {
        //            this.validateFormService.displayFieldErrors();
        //        }
    }
}
