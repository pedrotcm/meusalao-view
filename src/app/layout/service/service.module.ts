import { CategoryService } from '../../_services/category.service';
import { ProfessionalService } from '../../_services/professional.service';
import { ServiceService } from '../../_services/service.service';
import { OnlyNumberDirective } from '../../_shared/directives/onlynumber.directive';
import { CheckboxExtendedModule } from '../../_shared/modules/checkbox/checkbox.extended.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceManageComponent } from './manage/service-manage.component';
import { ServiceRoutingModule } from './service-routing.module';
import { ServiceComponent } from './service.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ServiceCadastreComponent } from './cadastre/service-cadastre.component';
import { InputMaskModule } from 'primeng/inputmask';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FieldsetModule } from 'primeng/fieldset';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { AccordionModule } from 'primeng/accordion';

@NgModule( {
    imports: [
        CommonModule,
        ServiceRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        InputTextModule,
        ButtonModule,
        TableModule,
        TooltipModule,
        ConfirmDialogModule,
        InputMaskModule,
        CheckboxModule,
        InputSwitchModule,
        FieldsetModule,
        CurrencyMaskModule,
        DialogModule,
        DropdownModule,
        AccordionModule,
        CheckboxExtendedModule
    ],
    declarations: [ ServiceManageComponent, ServiceComponent, ServiceCadastreComponent, OnlyNumberDirective ],
    providers: [ CategoryService, ServiceService, ProfessionalService ]
} )
export class ServiceModule { }
