import { Service } from '../../../_models/service';
import { ServiceService } from '../../../_services/service.service';
import { DatatableFilterService } from '../../../_shared/services/datatablefilter.service';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { ConfirmationService } from 'primeng/api';

@Component( {
    selector: 'app-manage',
    templateUrl: './service-manage.component.html',
    styleUrls: [ './service-manage.component.scss' ],
    animations: [ routerTransition ],
    providers: [ ConfirmationService ]
} )
export class ServiceManageComponent implements OnInit {

    results: Service[];
    datasource: Service[];
    cols: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;

    lastEvent: LazyLoadEvent;
    editEntity: Service;
    removeEntity: number;

    constructor(
        private confirmationService: ConfirmationService,
        private tableFilterService: DatatableFilterService,
        private serviceService: ServiceService
    ) { }

    ngOnInit() {
        this.loading = true;
        this.cols = [
            { field: 'name', header: 'Nome' },
            { field: 'commission', header: 'Comissão' },
            { field: 'timePrediction', header: 'Duração Prevista' },
            { field: 'visibleToClient', header: 'Agend. Online' },
            { field: 'prices', header: 'Valores' },
            { field: 'category', header: 'Categoria' },
        ];
    }

    edit( editEntity: Service ) {
        this.editEntity = editEntity;
    }

    remove( removeEntity: Service ) {
        this.confirmationService.confirm( {
            header: 'Confirmar exclusão',
            message: 'Deseja realmente excluir este serviço?',
            icon: 'fa fa-trash',
            accept: () => {
                this.removeEntity = removeEntity.id;
            },
            reject: () => {
            }
        } );
    }

    loadLazy( event: LazyLoadEvent ) {
        this.lastEvent = event;
        if ( !this.datasource ) {
            this.getServices();
        }

        if ( this.datasource ) {
            this.tableFilterService.filterProcess( this, event );
        }
    }

    updateTable( update ) {
        if ( update ) {
            this.loading = true;
            this.getServices();
        }
    }

    private getServices() {
        this.serviceService.getServices()
            .finally( () => this.loading = false )
            .subscribe( data => {
                this.datasource = data;
                this.tableFilterService.filterProcess( this, this.lastEvent );
            } );
    }
}
