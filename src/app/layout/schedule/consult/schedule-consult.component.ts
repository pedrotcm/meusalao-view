import { Client } from '../../../_models/client';
import { Professional } from '../../../_models/professional';
import { ParentScheduling } from '../../../_models/parentScheduling';
import { Status } from '../../../_models/status';
import { ClientService } from '../../../_services/client.service';
import { ProfessionalService } from '../../../_services/professional.service';
import { SchedulingService } from '../../../_services/scheduling.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { CLIENTS, PROFESSIONALS, PT_BR, STATUS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/components/common/api';
import { ConfirmationService } from 'primeng/api';
import { SelectItem } from 'primeng/components/common/api';

@Component( {
    selector: 'app-schedule-consult',
    templateUrl: './schedule-consult.component.html',
    styleUrls: [ './schedule-consult.component.scss' ],
    animations: [ routerTransition ],
    providers: [
        ValidateFormService, ConfirmationService
    ]
} )
export class ScheduleConsultComponent implements OnInit {

    consultForm: FormGroup;
    ptBR: any;
    filteredClients: any[];
    professionals: Professional[];
    clients: Client[];
    status: SelectItem[];

    results: ParentScheduling[];
    datasource: any[];
    cols: any[];
    colsServices: any[];
    loading: boolean;
    firstRegister: number;
    totalRecords = 0;
    rowsPerPage: number;
    display = false;

    schedulingSelected: ParentScheduling;

    constructor(
        private fb: FormBuilder,
        private schedulingService: SchedulingService,
        private professionalService: ProfessionalService,
        private clientService: ClientService,
        private confirmationService: ConfirmationService
    ) { }


    init() {
        this.consultForm = this.fb.group( {
            'dateScheduledStart': new FormControl( new Date() ),
            'dateScheduledEnd': new FormControl( new Date() ),
            'client': new FormControl(),
            'professional': new FormControl(),
            'status': new FormControl( Status.SCHEDULED )
        } );
    }
    ngOnInit() {
        this.init();
        this.ptBR = PT_BR;
        this.status = STATUS;
        this.cols = [
            { field: 'dateScheduled', header: 'Agendamento' },
            { field: 'client.name', header: 'Cliente' },
            { field: 'client.phone', header: 'Telefone' },
            { field: 'professional.nickname', header: 'Profissional' },
            { field: 'status', header: 'Status' }
        ];
        this.colsServices = [
            { field: 'description', header: 'Descrição' },
            { field: 'timePrediction', header: 'Tempo Previsto' },
            { field: 'value', header: 'Valor' }
        ];
        this.professionalService.getProfessionals().subscribe( data => {
            this.professionals = data;
        } );

        this.datasource = [];
    }

    filterClients( event ) {
        this.clientService.findByName( event.query.toLowerCase() ).subscribe( data => {
            this.filteredClients = data;
        } );
    }

    clearAutoCompleteProfessional( event ) {
        this.consultForm.controls[ 'professional' ].reset();
    }

    clearAutoCompleteClient( event ) {
        this.consultForm.controls[ 'client' ].reset();
    }

    onSubmit( { value, valid }: { value: any, valid: boolean } ) {
        this.loading = true;
        const clientId = value.client ? value.client.id : -1;
        const professionalId = value.professional ? value.professional.id : -1;
        this.schedulingService.findPaginated( value.dateScheduledStart, value.dateScheduledEnd, value.status, clientId, professionalId, 0, 10, '', 1 )
            .finally( () => this.loading = false )
            .subscribe( data => {
                this.results = data[ 'content' ];
                this.firstRegister = 1;
                this.rowsPerPage = 0 + this.results.length;
                this.totalRecords = data[ 'totalElements' ];
            } );
    }

    onClear() {
        this.init();
    }

    loadLazy( event: LazyLoadEvent ) {
        this.loading = true;
        const value = this.consultForm.value;
        const clientId = value.client ? value.client.id : -1;
        const professionalId = value.professional ? value.professional.id : -1;
        this.schedulingService.findPaginated( value.dateScheduledStart, value.dateScheduledEnd, value.status, clientId, professionalId, ( event.first / event.rows ), event.rows, event.sortField, event.sortOrder )
            .finally( () => this.loading = false )
            .subscribe( data => {
                this.results = data[ 'content' ];
                this.firstRegister = event.first + 1;
                this.rowsPerPage = event.first + this.results.length;
                this.totalRecords = data[ 'totalElements' ];
            } );
    }

    getStatus( status ) {
        switch ( status ) {
            case Status.SCHEDULED:
                return 'Agendado';
            case Status.ORDER_STARTED:
                return 'Comanda Iniciada';
            case Status.CANCELED:
                return 'Cancelado';
        }
    }

    edit( schedule: ParentScheduling ) {
        this.schedulingSelected = schedule;
        this.display = true;
    }

    openOrder( schedule: ParentScheduling ) {

    }
}
