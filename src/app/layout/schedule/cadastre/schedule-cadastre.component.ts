import { Client } from '../../../_models/client';
import { Professional } from '../../../_models/professional';
import { ParentScheduling } from '../../../_models/parentScheduling';
import { Scheduling } from '../../../_models/scheduling';
import { Service } from '../../../_models/service';
import { ServiceSelected } from '../../../_models/serviceSelected';
import { Status } from '../../../_models/status';
import { CategoryService } from '../../../_services/category.service';
import { ClientService } from '../../../_services/client.service';
import { ProfessionalService } from '../../../_services/professional.service';
import { SchedulingService } from '../../../_services/scheduling.service';
import { ServiceService } from '../../../_services/service.service';
import { ValidateFormService } from '../../../_shared/services/validate-form.service';
import { CustomDateFormatter } from '../../../_shared/utils/calendar/custom-date-formatter.provider';
import { CustomEventTitleFormatter } from '../../../_shared/utils/calendar/custom-event-title-formatter';
import { PROFESSIONALS, SERVICES, PT_BR, FREQUENCIES, PERIODS } from '../../../_shared/utils/database-fake';
import { routerTransition } from '../../../router.animations';
import { Component, OnInit, ViewChild, HostListener, ElementRef, AfterViewInit, TemplateRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DlDateTimePickerComponent, DateButton } from 'angular-bootstrap-datetimepicker';
import { SelectItem, SelectItemGroup } from 'primeng/components/common/api';
import { ConfirmationService } from 'primeng/api';
import * as moment from 'moment';
import { unitOfTime } from 'moment';
import { Subject } from 'rxjs/Subject';
import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours,
    addMinutes,
    isAfter
} from 'date-fns';

import * as $ from 'jquery';
import 'fullcalendar';
import 'fullcalendar-scheduler';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component( {
    selector: 'app-schedule-cadastre',
    templateUrl: './schedule-cadastre.component.html',
    styleUrls: [ './schedule-cadastre.component.scss' ],
    animations: [ routerTransition ],
    providers: [
        ValidateFormService, ConfirmationService
    ]
} )
export class ScheduleCadastreComponent implements OnInit, AfterViewInit, OnDestroy {

    //    dayStartHour: number;
    //    dayEndHour: number;
    ptBR = PT_BR;

    filtered: any[];
    filteredClients: any[];
    filteredServices: any[];

    professionals: Professional[];
    professionalsCadastre: any[];
    services: any[];
    servicesCadastre: any[];
    professionalsFilter: Professional[];
    professionalsLoadedView: Professional[] = [];
    clients: Client[];
    professionalSelected: Professional;
    display = false;
    displayProfissionalDialog = false;
    cadastreForm: FormGroup;
    cadastreUnavailableForm: FormGroup;


    activeDayIsOpen = false;
    todayDate = new Date();
    isEdit = false;
    headerDialog: string;

    frequencies: SelectItem[];
    hoursDisponible: SelectItem[];
    hoursUnavailable: SelectItem[];
    periods: SelectItem[];

    c: Date;
    dateSearch = new Date();
    dateScheduled: Date;

    isMobile: boolean;

    pageSchedule = 0;
    pageScheduleQuant: number;
    colsServices: any[];

    @ViewChild( 'dialogContent' )
    public dialog: TemplateRef<any>;
    modalReference: NgbModalRef;
    dialogTitle: string;

    @ViewChild( 'dialogUnavailable' )
    public dialogUnavailable: TemplateRef<any>;
    modalUnavailableReference: NgbModalRef;

    minTime = 8;
    maxTime = 20;

    blockedPanel: boolean;
    isDisabledDay: boolean;
    minScheduleDate: Date;

    constructor(
        private fb: FormBuilder,
        public validateFormService: ValidateFormService,
        public router: Router,
        private elRef: ElementRef,
        private professionalService: ProfessionalService,
        private clientService: ClientService,
        private serviceService: ServiceService,
        private categoryService: CategoryService,
        private modalService: NgbModal,
        private schedulingService: SchedulingService,
        private confirmationService: ConfirmationService
    ) {
        this.router.events.subscribe( val => {
            if ( val instanceof NavigationEnd && window.innerWidth <= 414 ) {
                this.isMobile = true;
            }
        } );
    }

    init() {
        this.servicesCadastre = [];
        this.professionalsCadastre = [];
        if ( this.professionals ) {
            this.professionalsCadastre.push( this.professionals );
        }
        if ( this.services ) {
            this.servicesCadastre.push( this.services );
        }
        this.cadastreForm = this.fb.group( {
            'id': new FormControl(),
            'idScheduling': new FormControl(),
            'client': new FormControl( null, Validators.required ),
            'dateScheduled': new FormControl( null ),
            'frequency': new FormControl( 'JUST_SCHEDULED' ),
            'scheduledServices': this.fb.array( [] ),
            'status': new FormControl( Status.SCHEDULED ),
        } );
        this.addServiceItem();

        this.cadastreUnavailableForm = this.fb.group( {
            'id': new FormControl(),
            'idScheduling': new FormControl(),
            'professional': new FormControl( null, Validators.required ),
            'dateUnavailable': new FormControl(),
            'period': new FormControl( 'ONLY_THIS_DATE' ),
            'startTimeUnavailable': new FormControl( null, Validators.required ),
            'endTimeUnavailable': new FormControl( null, Validators.required ),
            'reasonUnavailable': new FormControl()
        } );
        this.createHoursUnavailable();

    }



    ngOnInit() {
        const self = this;
        this.blockedPanel = true;
        this.init();
        this.serviceService.getServices().subscribe( data => {
            this.services = this.createServices( data, 0 );
        } );
        this.professionalService.getProfessionals()
            .subscribe( data => {
                data.sort( function ( a, b ) {
                    const textA = a.nickname.toUpperCase();
                    const textB = b.nickname.toUpperCase();
                    return textA.localeCompare( textB );
                } );
                this.professionalsCadastre.splice( 0, 1, data );
                this.professionals = data.slice();
                this.professionals.forEach( p => {
                    delete p.services;
                } );

                if ( sessionStorage.getItem( 'professionalsLoadedView' ) ) {
                    this.professionalsFilter = JSON.parse( sessionStorage.getItem( 'professionalsLoadedView' ) );
                } else {
                    this.professionalsFilter = data.slice();
                }

                if ( this.isMobile ) {
                    this.professionalsLoadedView.push( this.professionalsFilter[ 0 ] );
                    this.getSchedulings( this.professionalsFilter[ 0 ].id, this.professionalsFilter[ 0 ].nickname );
                } else {
                    for ( let i = 0; i < this.professionalsFilter.length; i++ ) {
                        if ( i < 3 ) {
                            this.professionalsLoadedView.push( this.professionalsFilter[ i ] );
                            this.getSchedulings( this.professionalsFilter[ i ].id, this.professionalsFilter[ i ].nickname );
                        }
                    }
                }
                sessionStorage.setItem( 'professionalsLoadedView', JSON.stringify( this.professionalsFilter ) );
                if ( ( this.isMobile && this.professionals.length > 1 ) || this.professionalsFilter.length > 3 ) {
                    $( '#slideRight' ).css( 'visibility', 'visible' );
                }
            } );

        $( '#calendar' ).fullCalendar( {
            schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
            header: {
                right: 'prev,next today',
                center: 'title',
                left: ''
            },
            buttonText: {
                today: 'Hoje',
                month: 'Mês',
                week: 'Semana',
                day: 'Dia',
                list: 'Lista'
            },
            resources: [],
            eventOverlap: true,
            themeSystem: 'bootstrap4',
            nowIndicator: true,
            defaultView: 'agendaDay',
            groupByResource: true,
            noEventsMessage: 'Nenhum agendamento encontrado',
            locale: 'pt-br',
            height: 'auto',
            allDaySlot: false,
            slotDuration: '00:10:00',
            slotLabelInterval: '00:10:00',
            slotLabelFormat: 'HH:mm',
            minTime: '07:30',
            maxTime: '20:40',
            businessHours: {
                dow: [ 1, 2, 3, 4, 5, 6 ],
                start: '08:00',
                end: '20:10'
            },
            timeFormat: 'HH:mm',
            editable: false,
            selectable: true,
            selectConstraint: 'businessHours',
            dayClick: function ( date, jsEvent, view, resource ) {
                const hours = date.get( 'hours' );
                const minutes = date.get( 'minutes' );
                const dateClicked = date.toDate();
                dateClicked.setHours( hours );
                dateClicked.setMinutes( minutes );
                dateClicked.setSeconds( 0 );
                dateClicked.setMilliseconds( 0 );
                const current = new Date();
                if ( dateClicked >= current && !self.isDisabledDay ) {
                    const startScheduled = hours * 60 + minutes;
                    self.isEdit = false;
                    self.dialogTitle = 'Novo Agendamento';
                    self.dateSearch.setHours( 0 );
                    self.dateSearch.setMinutes( 0 );
                    self.dateSearch.setSeconds( 0 );
                    self.createHoursDisponible( self.dateSearch <= current ? current : self.dateSearch );
                    const scheduledServices = [];
                    scheduledServices.push( {
                        service: null,
                        professional: null,
                        startTime: startScheduled
                    } );
                    self.cadastreForm.patchValue( {
                        dateScheduled: date.toDate(),
                        scheduledServices: scheduledServices
                    } );
                    const t = jsEvent.target;
                    if ( ( <any> t ).classList.contains( 'fc-nonbusiness' ) ) {
                        return;
                    } else if ( ( <any> t ).classList.contains( 'fc-bgevent' ) ) {
                        const unavailable = $( jsEvent.target ).data( 'store' ) as Scheduling;
                        self.isEdit = true;
                        self.cadastreUnavailableForm.patchValue( {
                            id: unavailable[ 'parentScheduling' ].id,
                            idScheduling: unavailable.id,
                            dateUnavailable: new Date( unavailable.dateScheduled ),
                            professional: unavailable.professional,
                            period: unavailable[ 'parentScheduling' ].period,
                            startTimeUnavailable: unavailable.startTimeUnavailable,
                            endTimeUnavailable: unavailable.endTimeUnavailable,
                            reasonUnavailable: unavailable.reasonUnavailable
                        } );
                        self.createEditUnavailable();
                    } else {
                        self.createEditScheduling();
                    }
                }
                //                alert( 'clicked ' + date.format() + ' on resource ' + resource.id );
            },
            select: function ( startDate, endDate, jsEvent, view, resource ) {
                $( '#calendar' ).fullCalendar( 'unselect' );
                //                alert('selected ' + startDate.format() + ' to ' + endDate.format() + ' on resource ' + resource.id);
            },
            eventClick: function ( calEvent, jsEvent, view ) {
                self.isEdit = true;
                self.dialogTitle = 'Editar Agendamento';
                const date = new Date();
                date.setHours( 0 );
                date.setMinutes( 0 );
                date.setSeconds( 0 );
                self.createHoursDisponible( date );
                const scheduling = calEvent.data as Scheduling;
                const scheduledServices = [];
                scheduledServices.push( {
                    service: scheduling.service,
                    professional: scheduling.professional,
                    startTime: scheduling.startTimeValue
                } );
                self.cadastreForm.patchValue( {
                    id: scheduling[ 'parentScheduling' ].id,
                    idScheduling: scheduling.id,
                    client: scheduling.client,
                    dateScheduled: new Date( scheduling.dateScheduled ),
                    scheduledServices: scheduledServices,
                    frequency: scheduling[ 'parentScheduling' ].frequency,
                } );
                self.cadastreForm.get( 'client' ).disable();
                self.createEditScheduling();

                // change the border color just for fun
                //                $( this ).css( 'border-color', 'red' );

            },
            resourceRender: function ( resourceObj, labelTds, bodyTds ) {
                //                console.log( resourceObj, labelTds, bodyTds );
                //                labelTds.append( '<button type="button" style="float:right;" class="icon-button fas fa-chevron-right" />' );
                //                $( '#calendar .fc-view-month td' ).append( '<input type="checkbox"/>AM<br>' );
            },
            viewRender: function ( currentView, element ) {
                if ( currentView.name === 'agendaDay' && $( '#header-schedule-professionals' ).length === 0 ) {
                    $( '#calendar > div.fc-view-container' )
                        .prepend( '<div id="header-schedule-professionals"><button id="slideLeft" type="button" style="visibility: hidden;" class="icon-button fa fa-chevron-left" /><button id="btnFilterProfessionals" type="button" class="icon-button" style="font-weight: bold;font-size:1em;width:80%;"><i class="fa fa-search"></i>    Profissionais</button><button id="slideRight" type="button" class="icon-button fa fa-chevron-right" style="visibility: hidden;" /></div>' );
                }
                self.dateSearch = currentView.intervalEnd.toDate();
                self.checkIsDisableDay( self.dateSearch );
            },
            eventRender: function ( event, element, view ) {
                if ( element[ 0 ].classList.contains( 'fc-bgevent' ) ) {
                    $( element ).data( 'store', event.store );
                    const reason = event.store.reasonUnavailable ? event.store.reasonUnavailable : 'Indisponível';
                    element.append( `<div class="fc-title-unavailable">${ reason }</div>` );
                }
                if ( event.data ) {
                    element.find( '.fc-content' )
                        .append( '<div style=\'padding: 0 0 0 5px\'>' + '- ' + event.data.service.name + '</div>' );
                }
            }
        } );

        this.frequencies = FREQUENCIES;
        this.periods = PERIODS;

        this.colsServices = [
            { field: 'service', header: 'Serviço' },
            { field: 'professional', header: 'Profissional' }
        ];
    }

    checkIsDisableDay( date ) {
        const day = moment( date ).isoWeekday();
        this.isDisabledDay = day === 7 ? true : false;
    }

    createServiceItem(): FormGroup {
        let nextStarTimeValue = null;
        if ( this.cadastreForm ) {
            if ( this.servicesItens.length > 0 && this.servicesItens[ 'controls' ][ this.servicesItens.length - 1 ][ 'controls' ].service.value ) {
                const time = this.servicesItens[ 'controls' ][ this.servicesItens.length - 1 ][ 'controls' ].service.value[ 'timePrediction' ];
                const startTime = this.servicesItens[ 'controls' ][ this.servicesItens.length - 1 ][ 'controls' ].startTime.value;
                const nextStartTime = Math.ceil( time / 10 ) * 10 + startTime;
                const startTimeHour = this.hoursDisponible.find( hour => {
                    if ( hour.value === nextStartTime ) {
                        return true;
                    }
                } );
                nextStarTimeValue = startTimeHour.value;
            }
        }
        return this.fb.group( {
            service: new FormControl( null, Validators.required ),
            //            professionals: this.fb.array( [ this.createProfessionalItem() ] ),
            professional: new FormControl( null, Validators.required ),
            startTime: new FormControl( nextStarTimeValue, Validators.required )
        } );
    }

    createProfessionalItem(): FormGroup {
        return this.fb.group( {
            professional: new FormControl( null, Validators.required )
        } );
    }

    get servicesItens(): FormArray {
        return this.cadastreForm.get( 'scheduledServices' ) as FormArray;
    }

    addServiceItem() {
        this.professionalsCadastre.splice( this.servicesItens.length, 1, this.professionals );
        this.servicesCadastre.splice( this.servicesItens.length, 1, this.services );
        this.servicesItens.push( this.createServiceItem() );
    }

    removeServiceItem() {
        this.servicesItens.removeAt( this.servicesItens.length - 1 );
    }

    addProfessionalItem( index ) {
        const professionals = this.servicesItens.controls[ index ].get( 'professionals' ) as FormArray;
        professionals.push( this.createProfessionalItem() );
    }

    removeProfessionalItem( indexService, indexProfessional ) {
        const professionals = this.servicesItens.controls[ indexService ].get( 'professionals' ) as FormArray;
        professionals.removeAt( indexProfessional );
    }

    ngAfterViewInit() {
        this.elRef.nativeElement.querySelector( '#btnFilterProfessionals' ).addEventListener( 'click', ( event ) => this.filterProfessionalDialog( event ) );
        this.elRef.nativeElement.querySelector( '#slideRight' ).addEventListener( 'click', ( event ) => this.slideRightClick( event ) );
        this.elRef.nativeElement.querySelector( '#slideLeft' ).addEventListener( 'click', ( event ) => this.slideLeftClick( event ) );
        this.elRef.nativeElement.querySelector( '.fc-prev-button' ).addEventListener( 'click', ( event ) => this.refreshSchedulings() );
        this.elRef.nativeElement.querySelector( '.fc-next-button' ).addEventListener( 'click', ( event ) => this.refreshSchedulings() );
        this.elRef.nativeElement.querySelector( '.fc-today-button' ).addEventListener( 'click', ( event ) => this.refreshSchedulings() );
    }

    filterProfessionalDialog( event ) {
        this.displayProfissionalDialog = true;
    }

    slideRightClick( event ) {
        const pageScheduleQuant = this.getQuantityProfessionalForPage( true );
        this.filterProfessionalsSchedulePage( this.pageSchedule, pageScheduleQuant );
    }

    slideLeftClick( event ) {
        const pageScheduleQuant = this.getQuantityProfessionalForPage( false );
        this.filterProfessionalsSchedulePage( this.pageSchedule, pageScheduleQuant );

    }

    filterProfessionalsSchedulePage( pageSchedule, pageScheduleQuant ) {
        if ( pageSchedule === 0 ) {
            $( '#slideLeft' ).css( 'visibility', 'hidden' );
        } else if ( pageSchedule > 0 ) {
            $( '#slideLeft' ).css( 'visibility', 'visible' );
        }
        const pageLength = this.isMobile ? pageScheduleQuant + 1 : pageScheduleQuant + 3;
        if ( pageLength >= this.professionalsFilter.length ) {
            $( '#slideRight' ).css( 'visibility', 'hidden' );
        } else {
            $( '#slideRight' ).css( 'visibility', 'visible' );
        }

        this.professionalsLoadedView = this.professionalsFilter.slice( pageScheduleQuant, this.isMobile ? pageScheduleQuant + 1 : pageScheduleQuant + 3 );
        sessionStorage.setItem( 'professionalsLoadedView', JSON.stringify( this.professionalsFilter ) );
        this.professionalsLoadedView.sort( function ( a, b ) {
            const textA = a.nickname.toUpperCase();
            const textB = b.nickname.toUpperCase();
            return textA.localeCompare( textB );
        } );
        const resourcesActiveds = $( '#calendar' ).fullCalendar( 'getResources' );
        for ( let j = 0; j < resourcesActiveds.length; j++ ) {
            $( '#calendar' ).fullCalendar( 'removeResource', resourcesActiveds[ j ] );
        }
        for ( let i = 0; i < this.professionalsLoadedView.length; i++ ) {
            this.getSchedulings( this.professionalsLoadedView[ i ].id, this.professionalsLoadedView[ i ].nickname );
            $( '#calendar' ).fullCalendar( 'addResource', { id: '' + this.professionalsLoadedView[ i ].id, title: this.professionalsLoadedView[ i ].nickname } );
        }
    }

    createServices( data, index ) {
        const servicesGroup: SelectItemGroup[] = [];
        let servicesItems;
        const categoryGroup = [];
        data.forEach( service => {
            const i = categoryGroup.indexOf( JSON.stringify( service.category ) );
            if ( i === -1 ) {
                servicesItems = [];
                servicesItems.push(
                    { label: service.name + ' - ' + service.timePrediction + 'min', value: service }
                );
                categoryGroup.push( JSON.stringify( service.category ) );
                servicesGroup.push(
                    {
                        label: service.category.description, value: service.category.id,
                        items: servicesItems
                    }
                );
            } else {
                const serviceGroup = servicesGroup[ i ];
                serviceGroup.items.push(
                    { label: service.name + ' - ' + service.timePrediction + 'min', value: service }
                );
                servicesGroup.splice( index, 1, serviceGroup );
            }
            delete service.category;
            delete service.professionals;
        } );
        //
        //        const categories = data.filter( category => {
        //            if ( category && category.services.length > 0 ) {
        //                return category;
        //            }
        //        } );
        //        categories.forEach( category => {
        //            const servicesItems = [];
        //            category.services.forEach( service => {
        //                servicesItems.push(
        //                    { label: service.name + ' - ' + service.timePrediction + 'min', value: service }
        //                );
        //            } );
        //            servicesGroup.push(
        //                {
        //                    label: category.description, value: category.id,
        //                    items: servicesItems
        //                }
        //            );
        //        } );
        this.servicesCadastre.splice( index, 1, servicesGroup );
        return servicesGroup;
    }

    getQuantityProfessionalForPage( isSlideRight: boolean ): number {
        if ( isSlideRight ) {
            return this.isMobile ? ++this.pageSchedule : ++this.pageSchedule * 3;
        } else {
            return this.isMobile ? --this.pageSchedule : --this.pageSchedule * 3;
        }
    }

    filterProfessionalsSchedule() {
        this.pageSchedule = 0;
        this.filterProfessionalsSchedulePage( this.pageSchedule, this.pageSchedule );
        this.displayProfissionalDialog = false;
    }

    filterClients( event ) {
        this.clientService.findByName( event.query.toLowerCase() ).subscribe( data => {
            this.filteredClients = data;
        } );
    }

    onSelectProfessional( professional: Professional ): any {
        this.cadastreForm.controls[ 'professional' ].reset( { value: professional, disabled: true } );
    }

    clearAutoComplete( event ) {
        this.professionalSelected = null;
    }

    clearAutoCompleteClient( event ) {
        this.cadastreForm.controls[ 'client' ].reset();
    }

    onShowDialog( e ) {
        this.headerDialog = 'Editar Agendamento';
        if ( !this.isEdit ) {
            this.headerDialog = 'Novo Agendamento';
        }
    }

    dateSearchFilter( date ) {
        this.refreshSchedulings();
        $( '#calendar' ).fullCalendar( 'gotoDate', moment( date ) );
    }

    addNewSchedule() {
        this.minScheduleDate = new Date();
        this.isEdit = false;
        this.dialogTitle = 'Novo Agendamento';
        this.dateSearch.setHours( 0 );
        this.dateSearch.setMinutes( 0 );
        this.dateSearch.setSeconds( 0 );
        this.createHoursDisponible( this.dateSearch <= this.minScheduleDate ? this.minScheduleDate : this.dateSearch );
        this.cadastreForm.patchValue( {
            dateScheduled: this.dateSearch
        } );
        this.createEditScheduling();
    }

    addNewUnavailableDate() {
        this.isEdit = false;
        this.cadastreUnavailableForm.patchValue( {
            dateUnavailable: this.dateSearch
        } );
        this.createEditUnavailable();
    }

    createHoursDisponible( data ) {
        const now = moment( data );
        const minutesNow = ( now.hour() * 60 ) + now.minute();
        this.hoursDisponible = [];
        for ( let i = this.minTime; i <= this.maxTime; i++ ) {
            for ( let m = 0; m <= 50; m += 10 ) {
                let label = i + ':' + m;
                if ( m === 0 ) {
                    label = i < 10 ? '0' + i + ':00' : i + ':00';
                } else {
                    label = i < 10 ? '0' + i + ':' + m : i + ':' + m;
                }
                const value = i * 60 + m;
                if ( value >= minutesNow ) {
                    this.hoursDisponible.push( { label: label, value: value } );
                }
                if ( i === this.maxTime ) {
                    break;
                }
            }
        }
    }

    createHoursUnavailable() {
        const now = moment( new Date() );
        now.set( { hour: 0, minute: 0, second: 0, millisecond: 0 } );
        const minutesNow = ( now.hour() * 60 ) + now.minute();
        this.hoursUnavailable = [];
        for ( let i = this.minTime; i <= this.maxTime; i++ ) {
            for ( let m = 0; m <= 50; m += 10 ) {
                let label = i + ':' + m;
                if ( m === 0 ) {
                    label = i < 10 ? '0' + i + ':00' : i + ':00';
                } else {
                    label = i < 10 ? '0' + i + ':' + m : i + ':' + m;
                }
                const value = i * 60 + m;
                if ( value >= minutesNow ) {
                    this.hoursUnavailable.push( { label: label, value: value } );
                }
                if ( i === this.maxTime ) {
                    break;
                }
            }
        }
    }


    onBlurServices( event ) {
        $( '#focus' ).focus();
    }

    onServiceChange( event, index ) {
        this.professionalService.findByService( event.value ).subscribe( data => {
            this.professionalsCadastre.splice( index, 1, data );
            //            serviceItem.removeControl( 'professional' );
            //            serviceItem.addControl(
            //                'professional', new FormControl( null, Validators.required )
            //                //                'professional', this.fb.array( [ this.createProfessionalItem() ] )
            //            );
        } );
    }

    onProfessionalChange( event, index ) {
        this.serviceService.findByProfessionalsId( event.value ).subscribe( data => {
            this.createServices( data, index );
            //            serviceItem.removeControl( 'professional' );
            //            serviceItem.addControl(
            //                'professional', new FormControl( null, Validators.required )
            //                //                'professional', this.fb.array( [ this.createProfessionalItem() ] )
            //            );
        } );
    }
    onSubmit( { value, valid }: { value: ParentScheduling, valid: boolean } ) {
        console.log( value );
        if ( valid ) {
            this.schedulingService.addScheduling( this.cadastreForm.getRawValue() ).subscribe( response => {
                if ( response ) {
                    this.refreshSchedulings();
                    this.modalReference.close();
                }
            } );
        }
        //        else {
        //            this.validateFormService.displayFieldErrors();
        //        }
    }

    onSubmitUnavailable( { value, valid }: { value: ParentScheduling, valid: boolean } ) {
        console.log( this.cadastreUnavailableForm.getRawValue() );
        if ( valid ) {
            this.schedulingService.addUnavailable( this.cadastreUnavailableForm.getRawValue() ).subscribe( response => {
                if ( response ) {
                    this.refreshSchedulings();
                    this.modalUnavailableReference.close();
                }
            } );
        }
        //        else {
        //            this.validateFormService.displayFieldErrors();
        //        }
    }

    //    get timeTotalPrevision(): number {
    //        let timeTotal;
    //        for ( const time of this.timePrevisions ) {
    //            if ( !timeTotal ) {
    //                timeTotal = 0;
    //            }
    //            timeTotal += time;
    //        }
    //        return timeTotal;
    //    }

    onFrequencyChange( event ) {
        console.log( event, this.cadastreForm.controls.frequency.value );
        const schedule = this.cadastreForm.controls.dateScheduled.value as Date;
        switch ( event.value ) {
            case 'JUST_SCHEDULED':
                const startTime = this.servicesItens[ 'controls' ][ 0 ][ 'controls' ].startTime.value;
                this.dateScheduled = new Date( schedule.getFullYear(), schedule.getMonth(), schedule.getDate() );
                //                this.dateScheduled.setMinutes( startTime );
                break;
            case 'WEEKLY':
                break;
            case 'FORTNIGHTLY':
                break;
            case 'MONTHLY':
                break;
        }
        console.log( this.dateScheduled );
    }

    ngOnDestroy(): void {
        if ( this.modalReference ) {
            this.modalReference.close();
        }
        if ( this.modalUnavailableReference ) {
            this.modalUnavailableReference.close();
        }
    }

    getSchedulings( idProfessional, nickname ) {
        let events = [];
        const tomorrow = new Date();
        tomorrow.setDate( tomorrow.getDate() + 1 );
        this.blockedPanel = true;
        $( '#calendar' ).fullCalendar( 'removeEvents' );
        this.schedulingService.findByProfessionalIdAndDateScheduledBetween(
            idProfessional,
            moment( this.dateSearch ).startOf( 'day' ).toDate(), moment( this.dateSearch ).endOf( 'day' ).toDate()
        ).subscribe( data => {
            data.forEach( scheduling => {
                if ( scheduling.isUnavailable ) {
                    events.push( {
                        id: scheduling.id,
                        resourceId: idProfessional,
                        start: scheduling.dateScheduled,
                        end: scheduling.dateEndPrevision,
                        rendering: 'background',
                        color: '#ff9f89',
                        store: scheduling
                    } );
                } else {
                    events.push( {
                        id: scheduling.id,
                        resourceId: idProfessional,
                        title: scheduling.client.name,
                        start: scheduling.dateScheduled,
                        end: scheduling.dateEndPrevision,
                        data: scheduling
                    } );
                }
            } );
            $( '#calendar' ).fullCalendar( 'renderEvents', events, true );
            this.blockedPanel = false;
        } );
        $( '#calendar' ).fullCalendar( 'addResource', { id: '' + idProfessional, title: nickname } );
    }

    refreshSchedulings() {
        for ( let i = 0; i < this.professionalsLoadedView.length; i++ ) {
            this.getSchedulings( this.professionalsLoadedView[ i ].id, this.professionalsLoadedView[ i ].nickname );
        }
    }

    createEditScheduling() {
        this.validateFormService.form = this.cadastreForm;
        this.modalReference = this.modalService.open( this.dialog, { centered: true, size: 'lg', keyboard: false, backdrop: 'static' } );
        this.modalReference.result.then( ( result ) => {
            this.init();
            $( 'body' ).removeClass( 'ui-overflow-hidden' );
            $( '.ui-widget-overlay' ).remove();
        }, ( reason ) => {
            this.init();
            $( 'body' ).removeClass( 'ui-overflow-hidden' );
            $( '.ui-widget-overlay' ).remove();
        } );
    }

    createEditUnavailable() {
        this.validateFormService.form = this.cadastreUnavailableForm;
        this.modalUnavailableReference = this.modalService.open( this.dialogUnavailable, { centered: true, size: 'lg', keyboard: false, backdrop: 'static' } );
        this.modalUnavailableReference.result.then( ( result ) => {
            this.init();
            $( 'body' ).removeClass( 'ui-overflow-hidden' );
            $( '.ui-widget-overlay' ).remove();
        }, ( reason ) => {
            this.init();
            $( 'body' ).removeClass( 'ui-overflow-hidden' );
            $( '.ui-widget-overlay' ).remove();
        } );
    }

    onFocusDateSearch( $event ) {
        $( '#focusDateSearch' ).focus();
    }

    onFocusUnavailableDateSearch( $event ) {
        $( '#focusUnavailableDateSearch' ).focus();
    }

    onScheduleDateSelected( data ) {
        this.createHoursDisponible( data );
    }

    cancelScheduling() {
        this.confirmationService.confirm( {
            header: 'Confirmar cancelamento',
            message: 'Deseja realmente cancelar este agendamento?',
            icon: 'fa fa-ban',
            accept: () => {
                this.schedulingService.cancelScheduling( this.cadastreForm.getRawValue() ).subscribe( response => {
                    if ( response ) {
                        this.refreshSchedulings();
                        this.modalReference.close();
                    }
                } );
            },
            reject: () => {
            }
        } );
    }

    deleteUnavailable( { value }: { value: ParentScheduling } ) {
        this.confirmationService.confirm( {
            header: 'Deletar indisponibilidade',
            message: 'Deseja realmente deletar esta indisponibilidade?',
            icon: 'fa fa-close',
            accept: () => {
                this.schedulingService.deleteUnavailable( value[ 'idScheduling' ] ).subscribe( response => {
                    this.refreshSchedulings();
                    this.modalUnavailableReference.close();
                } );
            },
            reject: () => {
            }
        } );
    }


}
