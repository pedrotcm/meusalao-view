import { ScheduleCadastreComponent } from './cadastre/schedule-cadastre.component';
import { ScheduleConsultComponent } from './consult/schedule-consult.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from './schedule.component';

const routes: Routes = [
    {
        path: '', component: ScheduleComponent,
        children: [
            { path: '', component: ScheduleCadastreComponent },
            { path: 'list', component: ScheduleConsultComponent }
        ]
    }
];

@NgModule( {
    imports: [ RouterModule.forChild( routes ) ],
    exports: [ RouterModule ]
} )
export class ScheduleRoutingModule {
}

