import { CategoryService } from '../../_services/category.service';
import { ClientService } from '../../_services/client.service';
import { ProfessionalService } from '../../_services/professional.service';
import { SchedulingService } from '../../_services/scheduling.service';
import { ServiceService } from '../../_services/service.service';
import { BlockableDivModule } from '../../_shared/modules/blockable-div/blockable-div.module';
import { CalendarHeaderModule } from '../../_shared/modules/calendar-header/calendar-header.module';
import { CheckboxExtendedModule } from '../../_shared/modules/checkbox/checkbox.extended.module';
import { MsgsModule } from '../../_shared/modules/messages/msgs.module';
import { CapitalizeFirstPipe } from '../../_shared/pipers/capitalizefirst.pipe';
import { MinuteToHour } from '../../_shared/pipers/minuteToHour';
import { MinuteToHourModule } from '../../_shared/pipers/minuteToHour.module';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ScheduleModule } from 'primeng/schedule';
import { CalendarModule } from 'angular-calendar';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import { ScheduleCadastreComponent } from './cadastre/schedule-cadastre.component';
import { ScheduleConsultComponent } from './consult/schedule-consult.component';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule as CalendarPrimengModule } from 'primeng/calendar';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { CheckboxModule } from 'primeng/checkbox';
import { BlockUIModule } from 'primeng/blockui';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

@NgModule( {
    imports: [
        CommonModule,
        ScheduleRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        ButtonModule,
        AutoCompleteModule,
        ScheduleModule,
        CalendarModule.forRoot(),
        CalendarHeaderModule,
        DialogModule,
        MultiSelectModule,
        DlDateTimePickerDateModule,
        DropdownModule,
        CalendarPrimengModule,
        TableModule,
        TooltipModule,
        CheckboxModule,
        CheckboxExtendedModule,
        MinuteToHourModule,
        BlockUIModule,
        BlockableDivModule,
        ConfirmDialogModule,
        MsgsModule
    ],
    declarations: [ ScheduleComponent, ScheduleCadastreComponent, ScheduleConsultComponent ],
    providers: [ ProfessionalService, ClientService, ServiceService, CategoryService, SchedulingService ]
} )
export class SchedulingModule { }
