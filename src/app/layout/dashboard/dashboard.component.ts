import { routerTransition } from '../../router.animations';
import { Component, OnInit } from '@angular/core';

@Component( {
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.scss' ],
    animations: [ routerTransition ]
} )
export class DashboardComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
