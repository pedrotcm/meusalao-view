import { Professional } from '../_models/professional';
import { Service } from '../_models/service';
import { MsgsService } from '../_shared/services/msgs.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class ProfessionalService {

    private professionalsUrl = '/api/professionals';

    constructor(
        private http: HttpClient,
        private msgsService: MsgsService,
    ) { }

    getProfessionals(): Observable<Professional[]> {
        return this.http.get<Professional[]>( this.professionalsUrl, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].professionals ),
            catchError( this.handleError( 'getProfessionals', [] ) )
            );
    }

    getProfessional( id: number ): Observable<Professional> {
        const url = `${ this.professionalsUrl }/${ id }`;
        return this.http.get<Professional>( url, httpOptions )
            .pipe(
            //      tap(_ => this.log(`fetched Professional id=${id} ${_}`)),
            catchError( this.handleError<Professional>( `getProfessional id=${ id }` ) )
            );
    }

    updateProfessional( Professional: Professional ): Observable<any> {
        return this.http.put( `${ this.professionalsUrl }/${ Professional.id }`, Professional, httpOptions ).pipe(
            //      tap(_ => this.log(`updated Professional id=${Professional.id}`)),
            catchError( this.handleError<any>( 'updateProfessional' ) )
        );
    }

    addProfessional( professional: Professional ): Observable<Professional> {
        const isEdit = professional.id ? true : false;
        return this.http.post<Professional>( this.professionalsUrl, professional, httpOptions ).pipe(
            tap( ( _: Professional ) => console.log( 'addProfessional', professional ) ),
            map( data => {
                isEdit ? this.msgsService.notifyEdit( 'Profissional' ) : this.msgsService.notifyRegister( 'Profissional' );
                return data;
            } ),

            catchError( this.handleError<Professional>( 'addProfessional' ) )
        );
    }

    deleteProfessional( Professional: Professional | number ): Observable<Professional> {
        const id = typeof Professional === 'number' ? Professional : Professional.id;
        const url = `${ this.professionalsUrl }/${ id }`;

        return this.http.delete<Professional>( url, httpOptions ).pipe(
            //      tap(_ => this.log(`deleted Professional id=${id}`)),
            map( data => {
                this.msgsService.notifyRemove( 'Professional' );
                return data;
            } ),
            catchError( this.handleError<Professional>( 'deleteProfessional' ) )
        );
    }

    findByNickname( term: string ): Observable<Professional[]> {
        if ( !term.trim() ) {
            // if not search term, return empty Professional array.
            return of( [] );
        }
        return this.http.get<Professional[]>( `${ this.professionalsUrl }/search/findByNickname?nickname=${ term }`, httpOptions ).pipe(
            map( data => data[ '_embedded' ].professionals ),
            //      tap(_ => this.log(`found Professionales matching "${term}"`)),
            catchError( this.handleError<Professional[]>( 'searchProfessionales', [] ) )
        );
    }

    findByService( service: Service | number ): Observable<Professional[]> {
        const id = typeof service === 'number' ? service : service.id;
        return this.http.get<Professional[]>( `${ this.professionalsUrl }/search/findByServicesId?id=${ id }`, httpOptions ).pipe(
            tap( data => console.log( data ) ),
            map( data => data[ '_embedded' ].professionals ),
            catchError( this.handleError<Professional[]>( 'findByService', [] ) )
        );
    }

    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.log( operation, error ); // log to console instead

            // TODO: better job of transforming error for user consumption
            //      this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of( result as T );
        };
    }


}
