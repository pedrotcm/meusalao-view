import { Category } from '../_models/category';
import { Professional } from '../_models/professional';
import { MsgsService } from '../_shared/services/msgs.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class CategoryService {

    private endpointUrl = '/api/categories';

    constructor(
        private http: HttpClient,
        private msgsService: MsgsService,
    ) { }


    getCategories(): Observable<Category[]> {
        return this.http.get<Category[]>( this.endpointUrl, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].categories ),
            catchError( this.handleError( 'getCategories', [] ) )
            );
    }

    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.log( operation, error ); // log to console instead

            // TODO: better job of transforming error for user consumption
            //      this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of( result as T );
        };
    }


}
