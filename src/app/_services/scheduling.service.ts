import { ParentScheduling } from '../_models/parentScheduling';
import { Professional } from '../_models/professional';
import { Status } from '../_models/status';
import { MsgsService } from '../_shared/services/msgs.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import * as moment from 'moment';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class SchedulingService {

    private entityName = 'Agendamento';
    private entityUnavailableName = 'Indisponibilidade';
    private endpointUrl = '/api/schedulings';

    constructor(
        private http: HttpClient,
        private msgsService: MsgsService,
    ) { }

    addScheduling( scheduling: ParentScheduling ): Observable<ParentScheduling> {
        const isEdit = scheduling.id ? true : false;
        return this.http.post<ParentScheduling>( this.endpointUrl, scheduling, httpOptions ).pipe(
            tap( ( _: ParentScheduling ) => console.log( 'addScheduling', scheduling ) ),
            map( data => {
                isEdit ? this.msgsService.notifyEdit( this.entityName ) : this.msgsService.notifyRegister( this.entityName );
                return data;
            } ),
            catchError( this.handleError<ParentScheduling>( 'addScheduling' ) )
        );
    }

    addUnavailable( scheduling: ParentScheduling ): Observable<ParentScheduling> {
        const isEdit = scheduling.id ? true : false;
        return this.http.post<ParentScheduling>( this.endpointUrl + '/unavailable', scheduling, httpOptions ).pipe(
            tap( ( _: ParentScheduling ) => console.log( 'addUnavailable', scheduling ) ),
            map( data => {
                isEdit ? this.msgsService.notifyEdit( this.entityUnavailableName ) : this.msgsService.notifyRegister( this.entityUnavailableName );
                return data;
            } ),
            catchError( this.handleError<ParentScheduling>( 'addUnavailable' ) )
        );
    }

    cancelScheduling( scheduling: ParentScheduling ): Observable<ParentScheduling> {
        const url = `${ this.endpointUrl }/cancel`;
        return this.http.post<ParentScheduling>( url, scheduling, httpOptions ).pipe(
            tap( ( _: ParentScheduling ) => console.log( 'addScheduling', scheduling ) ),
            map( data => {
                this.msgsService.notifyCancel( this.entityName );
                return data;
            } ),
            catchError( this.handleError<ParentScheduling>( 'cancelScheduling' ) )
        );
    }

    deleteUnavailable( unavailable: ParentScheduling | number ): Observable<ParentScheduling> {
        const id = typeof unavailable === 'number' ? unavailable : unavailable.id;
        const url = `${ this.endpointUrl }/${ id }`;

        return this.http.delete<ParentScheduling>( url, httpOptions ).pipe(
            //      tap(_ => this.log(`deleted Professional id=${id}`)),
            map( data => {
                this.msgsService.notifyRemove( this.entityUnavailableName );
                return data;
            } ),
            catchError( this.handleError<ParentScheduling>( 'deleteUnavailable' ) )
        );
    }

    findByProfessionalIdAndDateScheduledBetween( professional: Professional | number, dateStart: Date, dateEnd: Date ): Observable<ParentScheduling[]> {
        const id = typeof professional === 'number' ? professional : professional.id;
        const status = [ Status.SCHEDULED, Status.ORDER_STARTED, Status.UNAVAILABLE ];
        return this.http.get<ParentScheduling[]>(
            `${ this.endpointUrl }/search/findByStatusInAndProfessionalIdAndDateScheduledBetween?status=${ status }&idProfessional=${ id }&dateStart=${ dateStart }&dateEnd=${ dateEnd }`, httpOptions
        ).pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].schedulings ),
            //      tap(_ => this.log(`found Professionales matching "${term}"`)),
            catchError( this.handleError<ParentScheduling[]>( 'findByDateScheduledBetween', [] ) )
            );
    }

    getSchedulings(): Observable<ParentScheduling[]> {
        return this.http.get<ParentScheduling[]>( this.endpointUrl, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].schedulings ),
            catchError( this.handleError( 'getSchedulings', [] ) )
            );
    }

    findPaginated( startDate, endDate, status, clientId, professionalId, page, size, sortField, sortOrder ): Observable<any[]> {
        startDate = moment( startDate ).startOf( 'day' ).toDate();
        endDate = moment( endDate ).endOf( 'day' ).toDate();
        return this.http.get<ParentScheduling[]>(
            this.endpointUrl + `/paginated?startDate=${ startDate }&endDate=${ endDate }&status=${ status }&clientId=${ clientId }&professionalId=${ professionalId }&page=${ page }&size=${ size }&sortField=${ sortField }&sortOrder=${ sortOrder }`, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            //            map( data => data[ '_embedded' ].schedulings ),
            catchError( this.handleError( 'findPaginated', [] ) )
            );
    }

    deleteScheduling( scheduling: ParentScheduling | number ): Observable<ParentScheduling> {
        const id = typeof scheduling === 'number' ? scheduling : scheduling.id;
        const url = `${ this.endpointUrl }/${ id }`;

        return this.http.delete<ParentScheduling>( url, httpOptions ).pipe(
            //      tap(_ => this.log(`deleted Professional id=${id}`)),
            map( data => {
                this.msgsService.notifyRemove( this.entityName );
                return data;
            } ),
            catchError( this.handleError<ParentScheduling>( 'deleteScheduling' ) )
        );
    }


    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.log( operation, error ); // log to console instead

            // TODO: better job of transforming error for user consumption
            //      this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of( result as T );
        };
    }
}
