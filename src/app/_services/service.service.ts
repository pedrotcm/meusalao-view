import { Professional } from '../_models/professional';
import { Service } from '../_models/service';
import { MsgsService } from '../_shared/services/msgs.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class ServiceService {

    private entityName = 'Serviço';
    private endpointUrl = '/api/services';

    constructor(
        private http: HttpClient,
        private msgsService: MsgsService,
    ) { }

    addService( service: Service ): Observable<Service> {
        const isEdit = service.id ? true : false;
        return this.http.post<Service>( this.endpointUrl, service, httpOptions ).pipe(
            tap( ( _: Service ) => console.log( 'addService', service ) ),
            map( data => {
                isEdit ? this.msgsService.notifyEdit( this.entityName ) : this.msgsService.notifyRegister( this.entityName );
                return data;
            } ),
            catchError( this.handleError<Service>( 'addService' ) )
        );
    }

    getServices(): Observable<Service[]> {
        return this.http.get<Service[]>( this.endpointUrl, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].services ),
            catchError( this.handleError( 'getServices', [] ) )
            );
    }

    findByProfessionalsId( professional: Professional | number ): Observable<Service[]> {
        const id = typeof professional === 'number' ? professional : professional.id;
        return this.http.get<Service[]>(
            `${ this.endpointUrl }/search/findByProfessionalsId?idProfessional=${ id }`, httpOptions
        ).pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].services ),
            catchError( this.handleError( 'findByProfessionalsId', [] ) )
            );
    }

    deleteService( service: Service | number ): Observable<Service> {
        const id = typeof service === 'number' ? service : service.id;
        const url = `${ this.endpointUrl }/${ id }`;

        return this.http.delete<Service>( url, httpOptions ).pipe(
            //      tap(_ => this.log(`deleted Professional id=${id}`)),
            map( data => {
                this.msgsService.notifyRemove( this.entityName );
                return data;
            } ),
            catchError( this.handleError<Service>( 'deleteService' ) )
        );
    }


    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.log( operation, error ); // log to console instead

            // TODO: better job of transforming error for user consumption
            //      this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of( result as T );
        };
    }
}
