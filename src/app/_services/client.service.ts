import { Client } from '../_models/client';
import { MsgsService } from '../_shared/services/msgs.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders( { 'Content-Type': 'application/json' } )
};

@Injectable()
export class ClientService {

    private entityName = 'Cliente';
    private endpointUrl = '/api/clients';

    constructor(
        private http: HttpClient,
        private msgsService: MsgsService,
    ) { }

    addClient( client: Client ): Observable<Client> {
        const isEdit = client.id ? true : false;
        return this.http.post<Client>( this.endpointUrl, client, httpOptions ).pipe(
            tap( ( _: Client ) => console.log( 'addClient', client ) ),
            map( data => {
                isEdit ? this.msgsService.notifyEdit( this.entityName ) : this.msgsService.notifyRegister( this.entityName );
                return data;
            } ),
            catchError( this.handleError<Client>( 'addClient' ) )
        );
    }

    getClients(): Observable<Client[]> {
        return this.http.get<Client[]>( this.endpointUrl, httpOptions )
            .pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].clients ),
            catchError( this.handleError( 'getCLients', [] ) )
            );
    }

    deleteClient( client: Client | number ): Observable<Client> {
        const id = typeof client === 'number' ? client : client.id;
        const url = `${ this.endpointUrl }/${ id }`;

        return this.http.delete<Client>( url, httpOptions ).pipe(
            //      tap(_ => this.log(`deleted Professional id=${id}`)),
            map( data => {
                this.msgsService.notifyRemove( this.entityName );
                return data;
            } ),
            catchError( this.handleError<Client>( 'deleteClient' ) )
        );
    }

    findByName( term: string ): Observable<Client[]> {
        if ( !term.trim() ) {
            return of( [] );
        }
        return this.http.get<Client[]>( `${ this.endpointUrl }/search/findByName?name=${ term }`, httpOptions ).pipe(
            tap( data => {
                console.log( data );
            } ),
            map( data => data[ '_embedded' ].clients ),
            catchError( this.handleError<Client[]>( 'findByName', [] ) )
        );
    }


    private handleError<T>( operation = 'operation', result?: T ) {
        return ( error: any ): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.log( operation, error ); // log to console instead

            // TODO: better job of transforming error for user consumption
            //      this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of( result as T );
        };
    }
}
