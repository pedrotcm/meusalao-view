import { AuthenticationService } from './_services/authentication.service';
import { MsgsComponent } from './_shared/modules/messages/msgs.component';
import { MsgsService } from './_shared/services/msgs.service';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ValidateFormService } from './_shared/services/validate-form.service';
import { JwtInterceptor } from './jwt.interceptor';
import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

registerLocaleData( localePt, 'pt-BR' );

export function createTranslateLoader( http: HttpClient ) {
    return new TranslateHttpLoader( http, './assets/i18n/', '.json' );
}

@NgModule( {
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        TranslateModule.forRoot( {
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [ HttpClient ]
            }
        } ),
        AppRoutingModule,
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'pt-BR' }, AuthenticationService, CookieService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ],
    bootstrap: [ AppComponent ],
} )
export class AppModule { }
