import { Client } from './client';
import { Professional } from './professional';
import { ServiceSelected } from './serviceSelected';
import { Status } from './status';
export class ParentScheduling {
    id: number;
    client: Client;
    scheduledServices: ServiceSelected[];
    dateScheduled: Date;
    dateEndPrevision: Date;
    timeTotalPrevision: number;
    frequency: string;
    //    valueTotal: number;
    status: Status;
    isUnavailable: boolean;
}
