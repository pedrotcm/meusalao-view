import { People } from './people';
import { Service } from './service';
export class Professional extends People {
    nickname: string;
    rg: string;
    cpf: string;
    contractType: string;
    justArrivalOrder: boolean;
    services: Service[];
}
