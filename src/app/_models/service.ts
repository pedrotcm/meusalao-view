import { Category } from './category';
import { Price } from './price';
import { Professional } from './professional';
export class Service {
    id: number;
    name: string;
    commission: number;
    timePrediction: number;
    typeValue: string;
    visibleToClient: boolean;
    category: Category;
    prices: Price[];
}
