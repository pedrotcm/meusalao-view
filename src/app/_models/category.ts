import { Service } from './service';
export class Category {
    id: number;
    description: string;
    services: Service[];
}
