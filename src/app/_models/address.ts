export class Address {
    id: number;
    zipCode: string;
    address: string;
    neighborhood: string;
    city: string;
    state: string;
}
