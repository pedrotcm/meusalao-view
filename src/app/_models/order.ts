import { Client } from './client';
import { Professional } from './professional';
import { ServiceSelected } from './serviceSelected';
import { Status } from './status';
import { PaymentMethod } from './paymentMethod';

export class Order {
    id: number;
    orderNumber: number;
    client: Client;
    services: ServiceSelected[];
    dateOpened: Date;
    dateClosed: Date;
    datePaid: Date;
    valueTotal: number;
    timeTotalPrevision: number;
    status: Status;
    paymentMethods: PaymentMethod[];
    information: string;
}
