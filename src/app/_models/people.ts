import { Address } from './address';
import { Client } from './client';
export class People {
    id: number;
    name: string;
    birthday: string;
    phone: string;
    phone2: string;
    email: string;
    address: Address;
}
