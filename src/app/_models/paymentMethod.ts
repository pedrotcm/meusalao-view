export class PaymentMethod {
    id: number;
    description: string;
    tax: number;
}
