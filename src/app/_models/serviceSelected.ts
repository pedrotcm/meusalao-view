import { Price } from './price';
import { Professional } from './professional';
import { Service } from './service';
export class ServiceSelected {
    price: Price;
    professionals: Professional[];
    service: Service;
}
