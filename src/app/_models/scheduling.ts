import { Client } from './client';
import { Professional } from './professional';
import { Service } from './service';
import { Status } from './status';

export class Scheduling {
    id: number;
    client: Client;
    service: Service;
    professional: Professional;
    startTimeValue: number;
    dateScheduled: Date;
    dateEndPrevision: Date;
    timeTotalPrevision: number;
    frequency: string;
    //    valueTotal: number;
    status: Status;
    period: string;
    startTimeUnavailable: number;
    endTimeUnavailable: number;
    reasonUnavailable: string;
}
