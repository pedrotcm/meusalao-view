import { AuthenticationService } from '../_services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component( {
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ]
} )
export class LoginComponent implements OnInit {

    loading = false;
    error = '';
    loginForm: FormGroup;
    constructor(
        private router: Router,
        public authenticationService: AuthenticationService
    ) { }

    ngOnInit() {
        this.loginForm = new FormGroup( {
            username: new FormControl( null, Validators.required ),
            password: new FormControl( null, [ Validators.required, Validators.minLength( 8 ) ] )
        } );
    }

    login( { value, valid }: { value: any, valid: boolean } ): void {
        sessionStorage.removeItem( 'professionalsLoadedView' );
        this.loading = true;
        this.authenticationService.login( value.username, value.password )
            .subscribe( result => {
                console.log( result );
                if ( result === true ) {
                    // login successful
                    this.router.navigate( [ '/dashboard' ] );
                } else {
                    // login failed
                    this.error = 'Erro no login.';
                    this.loading = false;
                }
            }, error => {
                console.log( error );
                this.error = error;
                if ( error.status === 400 ) {
                    this.loginForm.patchValue( {
                        password: null
                    } );
                    this.error = 'Usuário ou senha incorretos.';
                } else {
                    this.error = 'Ocorreu um erro ao tentar acessar o sistema. Tente novamente.';
                }
                this.loading = false;
            } );

    }
}
