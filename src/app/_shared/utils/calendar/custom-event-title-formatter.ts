import { DatePipe } from '@angular/common';
import { LOCALE_ID, Inject } from '@angular/core';
import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';

export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
    // you can override any of the methods defined in the parent class
    constructor( @Inject( LOCALE_ID ) private locale: string ) {
        super();
    }
    //  monthTooltip(event: CalendarEvent): string {
    //    return;
    //  }

    //    weekTooltip( event: CalendarEvent ): string {
    //        return;
    //    }

    dayTooltip( event: CalendarEvent ): string {
        return;
    }

    month( event: CalendarEvent ): string {
        return `<b>${ new DatePipe( this.locale ).transform(
            event.start,
            'HH:mm',
            this.locale
        ) }</b> ${ event.title }`;
    }

    week( event: CalendarEvent ): string {
        return `<b>${ new DatePipe( this.locale ).transform(
            event.start,
            'HH:mm',
            this.locale
        ) }</b> ${ event.title }`;
    }

    day( event: CalendarEvent ): string {
        if ( event.meta.timeTotalPrevision <= 30 ) {
            return `${ event.title }`;
        }
        return `${ event.title }
            <p class="mb-0">
                Início:
                ${new DatePipe( this.locale ).transform(
                event.start,
                'HH:mm',
                this.locale
            ) }
            </p>
            <p class="mb-0">
                Término:
                ${new DatePipe( this.locale ).transform(
                event.end,
                'HH:mm',
                this.locale
            ) }
            </p>
        `;
    }

}
