import { CalendarDateFormatter, DateFormatterParams } from 'angular-calendar';
import { getISOWeek, startOfWeek, lastDayOfWeek } from 'date-fns';
import { DatePipe } from '@angular/common';

export class CustomDateFormatter extends CalendarDateFormatter {

    public monthViewTitle( { date, locale }: DateFormatterParams ): string {
        const year: string = new DatePipe( locale ).transform( date, 'y', locale );
        const month = new DatePipe( locale ).transform( date, 'LLLL', locale );
        return `${ month } de ${ year }`;
    }

    weekViewColumnSubHeader( { date, locale }: DateFormatterParams ): string {
        const day = new DatePipe( locale ).transform( date, 'dd', locale );
        const month = new DatePipe( locale ).transform( date, 'LLL', locale );
        return `${ day }/${ month }`;
    }


    public weekViewTitle( { date, locale }: DateFormatterParams ): string {
        const year: string = new DatePipe( locale ).transform( date, 'y', locale );
        const startDate = startOfWeek( date );
        const lastDate = lastDayOfWeek( date );
        const startMonth = new DatePipe( locale ).transform( startDate, 'LLL', locale );
        const startDay = new DatePipe( locale ).transform( startDate, 'dd', locale );
        const endMonth = new DatePipe( locale ).transform( lastDate, 'LLL', locale );
        const endDay = new DatePipe( locale ).transform( lastDate, 'dd', locale );
        if ( startMonth === endMonth ) {
            return `${ startDay } - ${ endDay } de ${ startMonth } de ${ year }`;
        } else {
            return `${ startDay } de ${ startMonth } - ${ endDay } de ${ endMonth } de ${ year }`;
        }
    }

    public dayViewTitle( { date, locale }: DateFormatterParams ): string {
        const year: string = new DatePipe( locale ).transform( date, 'y', locale );
        const month = new DatePipe( locale ).transform( date, 'LLLL', locale );
        const week = new DatePipe( locale ).transform( date, 'EEEE', locale );
        const day = new DatePipe( locale ).transform( date, 'dd', locale );
        return `${ week }, ${ day } de ${ month } de ${ year }`;
    }


    public dayViewHour( { date, locale }: DateFormatterParams ): string {
        //        return null;
        return new DatePipe( locale ).transform( date, 'HH:mm', locale );
    }
}
