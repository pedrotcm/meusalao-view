import { Category } from '../../_models/category';
import { Client } from '../../_models/client';
import { Order } from '../../_models/order';
import { Professional } from '../../_models/professional';
import { ParentScheduling } from '../../_models/parentScheduling';
import { Service } from '../../_models/service';
import { Status } from '../../_models/status';
import { PaymentMethod } from '../../_models/paymentMethod';
import { SelectItem } from 'primeng/components/common/api';

export const PT_BR: any = {
    firstDayOfWeek: 0,
    dayNames: [ 'domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado' ],
    dayNamesShort: [ 'dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb' ],
    dayNamesMin: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
    monthNames: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
    monthNamesShort: [ 'jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez' ],
    today: 'Hoje',
    clear: 'Limpar'
};

export const CLIENTS: Client[] = [
    { id: 1, name: 'Pedro Tomé de Carvalho Machado da Silva Costa', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 2, name: 'Ana Cristina Marreiros de Carvalho', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 3, name: 'Lyvia Ravenna Dias Cruz', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 4, name: 'Pedro Tomé 4', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 5, name: 'Ana Cristina 5', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 6, name: 'Lyvia Ravenna 6', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 7, name: 'Pedro Tomé 7', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 8, name: 'Ana Cristina 8', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 9, name: 'Lyvia Ravenna 9', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 10, name: 'Pedro Tomé 10', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 11, name: 'Ana Cristina 11', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 12, name: 'Lyvia Ravenna 12', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 13, name: 'Pedro Tomé 13', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null },
    { id: 14, name: 'Ana Cristina 14', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null }
];

export const PROFESSIONALS: Professional[] = [
    {
        id: 1, name: 'Pedro Tomé', nickname: 'Nickname', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 2, name: 'Ana Cristina', nickname: 'Nickname', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 3, name: 'Lyvia Ravenna', nickname: 'Nickname', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 4, name: 'Maria', nickname: 'Nickname', birthday: '16/09', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 5, name: 'Julia', nickname: 'Nickname', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 6, name: 'Ze', nickname: 'Nickname', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    },
    {
        id: 7, name: 'Carolina Miranda', nickname: 'Nickname', birthday: '18/01', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, phone2: null,
        rg: '2.337.249', cpf: '004.456.803-77', contractType: 'Comissão', justArrivalOrder: false, services: []
    }
];


export const CATEGORIES: Category[] = [
    {
        id: 1, description: 'Cabelo',
        services: [
            {
                id: 1, name: 'Corte', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
            {
                id: 2, name: 'Pintura', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
            {
                id: 11, name: 'Penteado', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: null, value: 60 },
                ]
            }
        ]
    },

    {
        id: 2, description: 'Depilação',
        services: [
            {
                id: 3, name: 'Linha', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
            {
                id: 4, name: 'Cera', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            }
        ]
    },
    {
        id: 3, description: 'Estética',
        services: [
            {
                id: 5, name: 'Massagem', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
        ]
    },
    {
        id: 4, description: 'Maquiagem',
        services: [
            {
                id: 1, name: 'Profissional', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
            {
                id: 2, name: 'Artística', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            }
        ]
    },
    {
        id: 5, description: 'Manicura e Pedicura',
        services: [
            {
                id: 1, name: 'Reflexologia das mãos', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            },
            {
                id: 2, name: 'Aplicação de unhas', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
                prices: [
                    { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                    { id: 12, complementOfName: 'Médio', value: 20 },
                    { id: 13, complementOfName: 'Grande', value: 30 }
                ]
            }
        ]
    }
];

export const SERVICES: Service[] = [
    {
        id: 1, name: 'Corte', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
        prices: [
            { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
            { id: 12, complementOfName: 'Médio', value: 20 },
            { id: 13, complementOfName: 'Grande', value: 30 }
        ]
    },
    {
        id: 2, name: 'Penteado', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 14, complementOfName: null, value: 20 }
        ]
    },
    {
        id: 3, name: 'Pintura', commission: 40, timePrediction: 60, visibleToClient: true, category: null, typeValue: null,
        prices: [
            { id: 15, complementOfName: 'Curto', value: 100.4 },
            { id: 16, complementOfName: 'Médio', value: 99 },
            { id: 17, complementOfName: 'Grande', value: 30.99 }
        ]
    },
    {
        id: 4, name: 'Penteado 2', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 18, complementOfName: null, value: 20 }
        ]
    },
    {
        id: 5, name: 'Penteado 3', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 19, complementOfName: null, value: 20 }
        ]
    },
    {
        id: 6, name: 'Penteado 4', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 20, complementOfName: null, value: 20 }
        ]
    },
    {
        id: 7, name: 'Penteado 5', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 21, complementOfName: null, value: 20 }
        ]
    },
    {
        id: 8, name: 'Penteado 6', commission: 10, timePrediction: 30, visibleToClient: false, category: null, typeValue: null,
        prices: [
            { id: 22, complementOfName: null, value: 20 }
        ]
    }
];

export const STATES: SelectItem[] = [
    { label: 'AC', value: 'AC' }, { label: 'AL', value: 'AL' }, { label: 'AP', value: 'AP' }, { label: 'AM', value: 'AM' }, { label: 'BA', value: 'BA' }, { label: 'CE', value: 'CE' },
    { label: 'DF', value: 'DF' }, { label: 'ES', value: 'ES' }, { label: 'GO', value: 'GO' }, { label: 'MA', value: 'MA' }, { label: 'MT', value: 'MT' }, { label: 'MS', value: 'MS' }, { label: 'MG', value: 'MG' }, { label: 'PA', value: 'PA' },
    { label: 'PB', value: 'PB' }, { label: 'PR', value: 'PR' }, { label: 'PE', value: 'PE' }, { label: 'PI', value: 'PI' }, { label: 'RJ', value: 'RJ' }, { label: 'RN', value: 'RN' }, { label: 'RS', value: 'RS' }, { label: 'RO', value: 'RO' },
    { label: 'RR', value: 'RR' }, { label: 'SC', value: 'SC' }, { label: 'SP', value: 'SP' }, { label: 'SE', value: 'SE' }, { label: 'TO', value: 'TO' }
];

export const PAYMENT_METHODS: PaymentMethod[] = [
    { id: 1, description: 'Dinheiro', tax: 0 },
    { id: 2, description: 'Cartão de Crédito', tax: 2.3 },
    { id: 3, description: 'Cartão de Débito', tax: 1.1 }
];

export const ORDERS: Order[] = [
    {
        id: 1,
        orderNumber: 1,
        client: {
            id: 1, name: 'Pedro Tomé 1', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null
        },
        services: [
            {
                price: { id: 1, complementOfName: 'Curtissimo', value: 10.5 },
                professionals: [ PROFESSIONALS[ 0 ], PROFESSIONALS[ 1 ] ],
                service: SERVICES[ 0 ]
            }
        ],
        dateOpened: new Date( 2018, 3, 24, 17, 3, 0 ),
        dateClosed: null,
        datePaid: null,
        valueTotal: 200,
        timeTotalPrevision: 100,
        status: Status.OPENED,
        paymentMethods: [ { id: 1, description: 'Cartão Crédito', tax: 2.3 }],
        information: 'Visa'
    },
    {
        id: 2,
        orderNumber: 2,
        client: {
            id: 1, name: 'Pedro Tomé 2', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null
        },
        services: [
            {
                price: { id: 1, complementOfName: 'Curtissimo', value: 10.5 },
                professionals: [ PROFESSIONALS[ 0 ], PROFESSIONALS[ 1 ] ],
                service: SERVICES[ 0 ]
            }
        ],
        dateOpened: new Date( 2018, 3, 24, 11, 11, 0 ),
        dateClosed: null,
        datePaid: null,
        valueTotal: 220,
        timeTotalPrevision: 100,
        status: Status.OPENED,
        paymentMethods: [ { id: 1, description: 'Cartão Crédito', tax: 2.3 }],
        information: 'Visa'
    },
    {
        id: 3,
        orderNumber: 3,
        client: {
            id: 1, name: 'Pedro Tomé 3', birthday: '10/02', phone: '(86) 99968-9393', email: 'pedr_tome@hotmail.com', address: null, observations: null, phone2: null, channelMetSalon: null
        },
        services: [
            {
                price: { id: 10, complementOfName: 'Curtissimo', value: 10.5 },
                professionals: [ PROFESSIONALS[ 0 ], PROFESSIONALS[ 1 ] ],
                service: SERVICES[ 0 ]
            }
        ],
        dateOpened: new Date( 2018, 3, 24, 12, 30, 0 ),
        dateClosed: null,
        datePaid: null,
        valueTotal: 230,
        status: Status.OPENED,
        timeTotalPrevision: 100,
        paymentMethods: [ { id: 1, description: 'Cartão Crédito', tax: 2.3 }],
        information: 'Visa'
    },
];

export const TYPE_VALUES: any[] = [
    { label: 'Único', value: 'UNIQUE' },
    { label: 'Variado', value: 'VARIED' },
];

export const CHANNELS_MET_SALON: any[] = [
    { label: 'TV', value: 'TV' },
    { label: 'E-mail', value: 'EMAIL' },
    { label: 'Facebook', value: 'FACEBOOK' },
    { label: 'Google', value: 'GOOGLE' },
    { label: 'Indicação de amigo', value: 'FRIEND_INDICATE' },
    { label: 'Instagram', value: 'INSTAGRAM' },
    { label: 'Rádio', value: 'RADIO' },
    { label: 'Outro', value: 'OTHER' },
];

export const FREQUENCIES: any[] = [
    { label: 'Somente a data agendada', value: 'JUST_SCHEDULED' }
    //    { label: 'Semanalmente', value: 'WEEKLY' },
    //    { label: 'Quinzenalmente', value: 'FORTNIGHTLY' },
    //    { label: 'Mensalmente', value: 'MONTHLY' }
];

export const PERIODS: any[] = [
    { label: 'Somente este dia', value: 'ONLY_THIS_DATE' }
];

export const STATUS: any[] = [
    { label: 'Agendado', value: 'SCHEDULED' },
    { label: 'Cancelado', value: 'CANCELED' },
    { label: 'Comanda Iniciada', value: 'ORDER_STARTED' }
];



