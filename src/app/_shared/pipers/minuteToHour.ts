import { Pipe, PipeTransform } from '@angular/core';

@Pipe( {
    name: 'minuteToHour'
} )
export class MinuteToHour implements PipeTransform {
    transform( value: number ): string {
        if ( !value ) {
            return null;
        }
        const m = value % 60;
        const h = ( value - m ) / 60;

        if ( value < 60 ) {
            return m.toString() + 'min';
        }

        return h.toString() + 'h' + ( m < 10 ? '0' : '' ) + m.toString() + 'min';

        //        if ( value > 0 && value / 60 < 1 ) {
        //            return value + 'min';
        //        } else {
        //            return value / 60 + 'h';
        //        }
    }
}
