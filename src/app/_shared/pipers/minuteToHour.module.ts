import { MinuteToHour } from './minuteToHour';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

@NgModule( {
    declarations: [ MinuteToHour ],
    exports: [ MinuteToHour ]
} )
export class MinuteToHourModule { }
