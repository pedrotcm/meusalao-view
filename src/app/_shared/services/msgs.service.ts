import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Message } from 'primeng/components/common/api';

type Severities = 'success' | 'info' | 'warn' | 'error';

@Injectable()
export class MsgsService {

    notificationGrowlChange: Subject<Object> = new Subject<Object>();
    notificationMsgChange: Subject<Object> = new Subject<Object>();
    notificationMsgChangeAll: Subject<Object> = new Subject<Object>();

    constructor() { }

    notifyMessage( severity: Severities, summary: string, detail: string ) {
        this.notificationMsgChange.next( { severity, summary, detail } );
        setTimeout( function () {
            //            const msgs = document.querySelector( 'p-messages .ui-messages' );
            const close = document.querySelector( '.ui-messages-close' );
            if ( close ) {
                ( <any> close ).click();
            }
        }, 5000 );

    }

    notifyAllMessage( errorsList: Message[] ) {
        this.notificationMsgChangeAll.next( errorsList );
    }

    notifyGrowl( severity: Severities, summary: string, detail: string, isSticky?: boolean, lifeTime?: number ) {
        this.notificationGrowlChange.next( { severity, summary, detail, isSticky, lifeTime } );
    }

    notifyRegister( entity: string ) {
        this.notifyMessage( 'success', null, `${ entity } cadastrado com sucesso.` );
    }

    notifyEdit( entity: string ) {
        this.notifyMessage( 'success', null, `${ entity } editado com sucesso.` );
    }

    notifyRemove( entity: string ) {
        this.notifyMessage( 'success', null, `${ entity } removido com sucesso.` );
    }

    notifyCancel( entity: string ) {
        this.notifyMessage( 'success', null, `${ entity } cancelado com sucesso.` );
    }

    clear() {
        this.notificationMsgChange.next( null );
        this.notificationMsgChangeAll.next( [] );
    }
}
