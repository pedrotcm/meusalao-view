import { Injectable } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/api';

@Injectable()
export class DatatableFilterService {

    constructor() { }

    filterProcess( ctx: any, event: LazyLoadEvent ) {
        if ( !ctx.datasource ) {
            return;
        }
        const filteredRows = ctx.datasource.filter( row => this.filterField( row, event.filters, ctx.cols ) );
        filteredRows.sort( ( a, b ) => this.compareField( a, b, event.sortField ) * event.sortOrder );
        ctx.results = filteredRows.slice( event.first, ( event.first + event.rows ) );
        ctx.firstRegister = event.first + 1;
        ctx.rowsPerPage = event.first + ctx.results.length;
        ctx.totalRecords = filteredRows.length;
    }

    private filterField( row, filter, cols ) {
        let isInFilter = false;
        let noFilter = true;

        for ( const columnName in filter ) {
            for ( let i = 0; i < cols.length; i++ ) {
                const column = cols[ i ];
                if ( row[ column.field ] == null ) {
                    continue;
                }
                noFilter = false;

                let rowValue: String = row[ column.field ].toString().toLowerCase();
                if ( typeof row[ column.field ] === 'boolean' ) {
                    rowValue = row[ column.field ] ? 'sim' : 'não';
                } if ( typeof row[ column.field ] === 'object' ) {
                    rowValue = JSON.stringify( row[ column.field ] ).toLowerCase();
                }

                const filterMatchMode: String = filter[ columnName ].matchMode;

                if ( filterMatchMode.includes( 'contains' ) && rowValue.includes( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'startsWith' ) && rowValue.startsWith( filter[ columnName ].value.toLowerCase() ) ) {
                    isInFilter = true;
                } else if ( filterMatchMode.includes( 'in' ) && filter[ columnName ].value.includes( rowValue ) ) {
                    isInFilter = true;
                }
            }
        }
        if ( noFilter ) {
            isInFilter = true;
        }
        return isInFilter;
    }

    private compareField( rowA, rowB, field: string ): number {
        if ( rowA[ field ] == null ) {
            if ( rowA[ 'id' ] > rowB[ 'id' ] ) {
                return 1;
            } else {
                return -1;
            }
        }
        if ( typeof rowA[ field ] === 'string' ) {
            return rowA[ field ].localeCompare( rowB[ field ] );
        }
        if ( typeof rowA[ field ] === 'number' ) {
            if ( rowA[ field ] > rowB[ field ] ) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
