import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxExtendedComponent } from './checkbox-extended.component';

@NgModule( {
    imports: [ CommonModule ],
    declarations: [ CheckboxExtendedComponent ],
    exports: [ CheckboxExtendedComponent ],
} )
export class CheckboxExtendedModule { }
