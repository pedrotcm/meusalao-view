import { Component, forwardRef } from '@angular/core';
import { Checkbox } from 'primeng/primeng';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

const CHECKBOX_VALUE_ACCESOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef( () => CheckboxExtendedComponent ),
    multi: true
};

@Component( {
    selector: 'app-checkbox',
    providers: [ CHECKBOX_VALUE_ACCESOR ],
    template: `
          <div [ngStyle]="style" [ngClass]="'ui-chkbox ui-widget'" [class]="styleClass">
              <div class="ui-helper-hidden-accessible">
                  <input #cb type="checkbox" [attr.id]="inputId" [name]="name" [value]="value" [checked]="checked"
                            (focus)="onFocus($event)" (blur)="onBlur($event)"
                  [ngClass]="{'ui-state-focus':focused}" (change)="handleChange($event)" [disabled]="disabled" [attr.tabindex]="tabindex">
              </div>
              <div class="ui-chkbox-box ui-widget ui-corner-all ui-state-default" (click)="onClick($event,cb,true)"
                          [ngClass]="{'ui-state-active':checked,'ui-state-disabled':disabled,'ui-state-focus':focused}">
                  <span class="ui-chkbox-icon ui-clickable" [ngClass]="{'fa fa-check':checked}"></span>
              </div>
          </div>
          <label class="ui-chkbox-label" (click)="onClick($event,cb,true)"
                  [ngClass]="{'ui-label-active':checked, 'ui-label-disabled':disabled, 'ui-label-focus':focused}"
                  *ngIf="label" [attr.for]="inputId">{{label}}</label>
      `,
} )
export class CheckboxExtendedComponent extends Checkbox {


    writeValue( model: any ) {
        this.model = model;
        this.checked = this.isCheckedValue();
    }

    removeValue() {
        const index = this.model.findIndex( value => {
            //            return JSON.stringify( value ) === JSON.stringify( this.value );
            return value.id === this.value.id;
        } );
        if ( index >= 0 ) {
            this.model.splice( index, 1 );
        }
    }

    addValue() {
        this.model.push( this.value );
    }

    isCheckedValue() {
        return this.model && this.contains( this.model, this.value );
    }

    contains( model, item ) {
        for ( let i = 0; i < model.length; i++ ) {
            //            if ( JSON.stringify( model[ i ] ) === JSON.stringify( item ) ) {
            if ( model[ i ].id === item.id ) {
                return true;
            }
        }
        return false;
    }

}

