import { CapitalizeFirstPipe } from '../../pipers/capitalizefirst.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarHeaderComponent } from './calendar-header.component';
import { CalendarModule } from 'angular-calendar';

@NgModule( {
    imports: [
        CommonModule,
        CalendarModule
    ],
    declarations: [ CalendarHeaderComponent, CapitalizeFirstPipe ],
    exports: [ CalendarHeaderComponent ]
} )
export class CalendarHeaderModule { }
