import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import * as moment from 'moment';
import {
    isSameDay
} from 'date-fns';

@Component( {
    selector: 'app-calendar-header',
    templateUrl: './calendar-header.component.html',
    styleUrls: [ './calendar-header.component.scss' ]
} )
export class CalendarHeaderComponent implements OnInit {


    @Input() activeDayIsOpen: boolean;

    @Input() view: string;

    @Input() viewDate: Date;

    @Input() locale: string;

    @Output() activeDayIsOpenChange: EventEmitter<boolean> = new EventEmitter();

    @Output() viewChange: EventEmitter<string> = new EventEmitter();

    @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();

    today = new Date();

    constructor() { }

    ngOnInit() {
    }

    isToday(): boolean {
        return isSameDay( this.viewDate, this.today );
    }

}
