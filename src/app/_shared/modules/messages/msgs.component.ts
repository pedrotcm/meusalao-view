import { MsgsService } from '../../services/msgs.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { Subscription } from 'rxjs/Subscription';

@Component( {
    selector: 'app-msgs',
    templateUrl: './msgs.component.html',
    styleUrls: [ './msgs.component.scss' ]
} )
export class MsgsComponent implements OnInit, OnDestroy {

    msgs: Message[] = [];
    msgsGrowl: Message[] = [];
    isSticky = false;
    lifeTime = 10000;

    subscriptionMsg: Subscription;
    subscriptionAllMsg: Subscription;
    subscriptionGrowl: Subscription;

    constructor( private msgsService: MsgsService ) { }

    ngOnInit() {
        this.subscribeToGrowl();
        this.subscribeToMessages();
        this.subscribeToAllMessages();
    }

    subscribeToMessages() {
        this.subscriptionMsg = this.msgsService.notificationMsgChange
            .subscribe( notification => {
                this.msgs.length = 0;
                this.msgs.push( notification );
            } );
    }

    subscribeToAllMessages() {
        this.subscriptionAllMsg = this.msgsService.notificationMsgChangeAll
            .subscribe( ( notification: Message[] ) => {
                this.msgs.length = 0;
                for ( let i = 0; i < notification.length; i++ ) {
                    this.msgs.push( notification[ i ] );
                }
            } );
    }

    subscribeToGrowl() {
        this.subscriptionGrowl = this.msgsService.notificationGrowlChange
            .subscribe( notification => {
                const notificationObject = <MsgsComponent> notification;
                this.isSticky = notificationObject.isSticky ? notificationObject.isSticky : this.isSticky;
                this.lifeTime = notificationObject.lifeTime ? notificationObject.lifeTime : this.lifeTime;
                this.msgsGrowl = [];
                this.msgsGrowl.push( notification );
            } );
    }

    ngOnDestroy() {
        this.subscriptionGrowl.unsubscribe();
        this.subscriptionMsg.unsubscribe();
        this.subscriptionAllMsg.unsubscribe();
    }

}
