import { MsgsService } from '../../services/msgs.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MsgsComponent } from './msgs.component';
import { MessagesModule } from 'primeng/messages';
import { GrowlModule } from 'primeng/growl';

@NgModule( {
    imports: [
        CommonModule,
        MessagesModule,
        GrowlModule
    ],
    declarations: [ MsgsComponent ],
    exports: [ MsgsComponent ],
    providers: [ MsgsService ],
} )
export class MsgsModule { }
