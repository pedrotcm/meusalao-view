import { BlockableDivComponent } from './blockable-div.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule( {
    imports: [
        CommonModule
    ],
    declarations: [ BlockableDivComponent ],
    exports: [ BlockableDivComponent ]
} )
export class BlockableDivModule { }
